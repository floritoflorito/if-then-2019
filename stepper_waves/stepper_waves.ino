// Accelerometer
#include <Wire.h>
#include <MPU6050_tockn.h>
MPU6050 mpu6050(Wire);
//--------------------------------------
#define ID 2
//Define pin connections & motor's steps per revolution
// Motor steps per rotation
#define STEPS_PER_REV 200
#define FULLSTEP_DEGREE 1.8f

#define STEPPER_MINIMUM_MICROSECONDS 350

int stepperDivider = 1;

// unscaled delay time as given by user
int stepperDelayMicroseconds = 600; // fastest=350..3000 already quite slow

// actual delay time used
// one will be set through stepperDelayMicroseconds and stepeprDivider in stepper_updateActualDelayTime with min 350
int stepperActualDelayTime = 200;

boolean stepperForward = true;

boolean stepperContinuousTurning = false;
float stepperTargetTurnDegrees = 0;



const int stepsPerRevolution = 200;

//// ACCELEROMETER
//const int MPU_addr = 0x68;
//int16_t AcX, AcY, AcZ, Tmp, GyX, GyY, GyZ;
//int minVal = 265;
//int maxVal = 402;
double degX = 0;
double Vel_degX=0;
double lastDegX = 0;
int deg0=0;
//boolean accelerator_autoSendData = false;
//int accelerator_autoSendDataFrameCounter = 0;
//int accelerator_autoSendDataFrames = 5;
//int accelerator_autoSendBytes = 1;


// STEPPER STUFF
#define MODE_0_PIN 2
#define MODE_1_PIN 3
#define DIR_PIN 8
#define STEP_PIN 12

void setup() {
Serial.begin(9600);
  
  Wire.begin();
  mpu6050.begin();
  mpu6050.calcGyroOffsets(true);
  // Setup the pins as Outputs
  pinMode(MODE_0_PIN, OUTPUT);
  pinMode(MODE_1_PIN, OUTPUT);
  pinMode(STEP_PIN, OUTPUT);
  pinMode(DIR_PIN, OUTPUT);
//
//  SetupAccelerometer();
//  ResetAccelerometerSettings();
//   ReadAccelerometer();
//  stepper_reset();
  stepperContinuousTurning = false;
  stepperTargetTurnDegrees = 0;
  stepper_InitDivider(4);
  stepper_setDirectionForward(true);
  stepper_updateActualDelayTime();
}

void loop() {
  mpu6050.update();
float y=mpu6050.getAngleX();
float stepss= 25* (y-lastDegX);
 lastDegX=y;
// stepss=0;
float vel_y =stepss/25;//mpu6050.getGyroX();
int threshold=8;
//stepss = vel_y;
if (vel_y >= threshold){
  Serial.print(vel_y);
   Serial.print("  Right");
  Serial.print("   ");
 stepper_setDirectionForward(true);
 }
 else if (vel_y <= - threshold){
  Serial.print(vel_y);
   Serial.print("  Left");
  Serial.print("   "); 
 stepper_setDirectionForward(false);
  }
 else {
  Serial.print(vel_y);
  Serial.print("STILL"); 
  Serial.print("   "); 
   //stepss=0;
   stepss=0;
 }
int Stepss=stepss;
// stepss=900;
// stepss=stepss;
   for (int x = 0; x < abs(round(Stepss)); x++) {
      digitalWrite(STEP_PIN, HIGH);
      delayMicroseconds(stepperActualDelayTime);//(1+mpu6050.getGyroY()));
      digitalWrite(STEP_PIN, LOW);
      delayMicroseconds(stepperActualDelayTime);
    }
delay(50);

 Serial.print(Stepss);
  Serial.print("   ");
 Serial.println(y);
// delay(3000);
//
// parseSerial();
//accelerator_autoSendData=0;
//  if (accelerator_autoSendData) {
//
//    accelerator_autoSendDataFrameCounter++;
//    if (accelerator_autoSendDataFrameCounter >= accelerator_autoSendDataFrames) {
//      accelerator_autoSendDataFrameCounter = 0;
//      ReadAccelerometer();
//
//      switch (accelerator_autoSendBytes) {
//        case 1: serialSendOneByteAccelerometer(); break;
//        case 2: serialSendTwoBytesAccelerometer(); break;
//      }
//    }
//
//  }
////  Serial.println(Vel_degX);
////stepperContinuousTurning=1;
//  if (stepperContinuousTurning) {
//    digitalWrite(STEP_PIN, HIGH);
//    delayMicroseconds(stepperActualDelayTime);
//    digitalWrite(STEP_PIN, LOW);
//    delayMicroseconds(stepperActualDelayTime);
//  }
//
//  else if (stepperTargetTurnDegrees != 0) {
//
//    int steps = (int)(stepperTargetTurnDegrees / FULLSTEP_DEGREE * stepperDivider);
//
//    // Spin motor one rotation slowly
//    for (int x = 0; x < steps; x++) {
//      digitalWrite(STEP_PIN, HIGH);
//      delayMicroseconds(stepperActualDelayTime);
//      digitalWrite(STEP_PIN, LOW);
//      delayMicroseconds(stepperActualDelayTime);
//    }
//
//    stepperTargetTurnDegrees = 0;
//
//  }

  
}

void my_Stepper()
{
  int dirPin,stepPin;
  
  // Set motor direction clockwise
  digitalWrite(dirPin, HIGH);
//digitalWrite(dirPin, LOW);
  // Spin motor slowly
  for(int x = 0; x < stepsPerRevolution; x++)
  {
    digitalWrite(stepPin, HIGH);
    delayMicroseconds(2000);
    digitalWrite(stepPin, LOW);
    delayMicroseconds(2000);
  }
  delay(1000); // Wait a second
  
  // Set motor direction counterclockwise
//  digitalWrite(dirPin, LOW);

  // Spin motor quickly
  for(int x = 0; x < stepsPerRevolution; x++)
  {
    digitalWrite(stepPin, HIGH);
    delayMicroseconds(1000);
    digitalWrite(stepPin, LOW);
    delayMicroseconds(1000);
  }
  delay(1000); // Wait a second
}

/*
MODE2  MODE1  MODE0    DIVIDER
   0      0      0        full step (2-phase excitation)
   0      0      1        half step (1-2 phase excitation)
   0      1      0        1/4 step (W1-2 phase excitation)
   0      1      1        8 microsteps / step
*/
  
void stepper_reset() {
  stepperContinuousTurning = false;
  stepperTargetTurnDegrees = 0;
  stepper_InitDivider(4);
  stepper_setDirectionForward(true);
  stepper_updateActualDelayTime();
}

void stepper_InitDivider(int newDivider) {
  if (newDivider == 1) {
    stepperDivider = newDivider;
    digitalWrite(MODE_0_PIN, LOW);
    digitalWrite(MODE_1_PIN, LOW);
    stepper_updateActualDelayTime();
  }
  else if (newDivider == 2) {
    stepperDivider = newDivider;
    digitalWrite(MODE_0_PIN, HIGH);
    digitalWrite(MODE_1_PIN, LOW);
//     digitalWrite(MODE_0_PIN, LOW);
//    digitalWrite(MODE_1_PIN, HIGH);
    stepper_updateActualDelayTime();
  }
  else if (newDivider == 4) {
    stepperDivider = newDivider;
    digitalWrite(MODE_0_PIN, LOW);
    digitalWrite(MODE_1_PIN, HIGH);
    stepper_updateActualDelayTime();
  }
  else if (newDivider == 8) {
    stepperDivider = newDivider;
    digitalWrite(MODE_0_PIN, HIGH);
    digitalWrite(MODE_1_PIN, HIGH);
    stepper_updateActualDelayTime();
  }

}


void stepper_setDirectionForward(boolean b) {
  if (b) {
    // Set motor direction clockwise
    digitalWrite(DIR_PIN, HIGH);
  } else {
    digitalWrite(DIR_PIN, LOW);
  }
}



void stepper_updateActualDelayTime() {
  stepperActualDelayTime = max(STEPPER_MINIMUM_MICROSECONDS, stepperDelayMicroseconds / stepperDivider);
}
