//spacedList.js

inlets=4;
outlets=2;

var spacing=1;
var offset=0;
var ledindex0;
var ledindex1;

function bang() {
	if (inlet==0) {
		var out=new Array();
		for (var index=offset;index<ledindex1;index+=spacing) {
			out.push(index);
		}
		outlet(0, out);
		outlet(1, "bang");
	}
}

function msg_int(i) {
	if (inlet==1) {
		spacing=i;
		//post("spacing="+spacing+"\n");
	}
	else if (inlet==2) {
		offset=i;
		//post("offset="+offset+"\n");
	}
}

function list() {
	if (inlet==3)  {
		var l = arrayfromargs(arguments);
		ledindex0 = l[0];
		ledindex1 = l[1];
		//post("ledindex0="+ledindex0+", ledindex1="+ledindex1+"\n");
	}
}