# README #

This README would normally document whatever steps are necessary to get your application up and running.

### TODO Converting If/Then 2018 to If/Then 2019 ###

Serial protocol If/Then 2018:

commands to send: (found in SerialMain.amxd->serialThingy->SERIAL->scid->SerialControlHelp)
* requestaccone: request accelerometer one byte
* requestacctwo: request accelerometer two bytes
* autosend [0/1]: automatically sending accelerometer data
* autosendframes [1-128]: every nth frame to send the accelerometer data
* autosendbytes [1/2]: send the accelerometer data in one/two bytes

* reset
* divider [0-3] (= divider 1,2,4,8)
* delaylo [350-2382]: stepper pulse delay in microseconds
* delayhi [0-16383]: stepper pulse delay in microseconds
* targetdeglo [2.812-360]: target delta degrees to step
* targetdeghi [0-409.6]:   target delta degrees to step
* forward: change direction forward
* reverse: change direction reverse

commands to receive:
* id [int] continuous [0/1]
* id [int] requestaccone [0.0..360.0]
* id [int] reqiestacctwo [0.0..360.0]

### FOUND IN ARDUINO ###

We will approach it like a MIDI device:

   Incoming messages:

   [1][x][x][x][x][x][x][x] [0][x][x][x][x][x][x][x] [0][x][x][x][x][x][x][x]
   byte0                    byte1                    opt. bytes

   The first byte (byte0) will be the status byte and tell us what type of message is coming and how long it is.

   11111111 (255): RESET
   -> will return an answer 10000110 (134): continuousTurning has been switched off!

   11100000 (224): request ID
   -> will return an answer 11100001 (225): answer ID with one byte containing the ID (0..127)


   11000000 (192): set divider to 1
   11000001 (193): set divider to 2
   11000010 (194): set divider to 4
   11000011 (195): set divider to 8

   10000000 (128): set stepper delay microseconds with one byte 0..127 -> 350..2382 (﻿STEPPER_MINIMUM_MICROSECONDS + byte×16)
   10000001 (129): set stepper delay microseconds with two bytes (MSB,LSB) 0..16383 -> 0..16384 (values will clip at STEPPER_MINIMUM_MICROSECONDS)
   delay microseconds will adjust for microsteps, meaning that with microsteps these microseconds will become smaller
   the user doesn't have to account for this

   10000010 (130): set stepper forward
   10000011 (131): set stepper backward

   10000100 (132): set stepper continuousTurning on
   10000101 (133): set stepper continuousTurning off

   10001000 (136): set stepper target degrees with one byte (0..127) --[+1]--> (1..128) --[*2.8125]--> ( 2.8125..360 )
                   (also turns off continuousTurning)
   -> will return an answer 10000110 (134): continuousTurning has been switched off!
   10001000 (137): set stepper target degrees with two bytes (0..3600..7200..14400..16383) --[/40]--> (0..90..180..360..﻿409,575)
                   (also turns off continuousTurning)
                   3600 => 28 16 , 7200 => 56 32 , 14400 => 112 64
   -> will return an answer 10000110 (134): continuousTurning has been switched off!

   11110000 (240): request one byte degrees from accelerometer
   -> will respond with 11110001 (241): plus one byte 0..360deg -> 0..127
   11110010 (242): request two byte degrees from accelerometer
   -> will respond with 11110011 (243): plus two bytes 0..360deg -> 0..16383
   244
   -> 245
   246
   -> 247

   11111000 (248): auto-send accelerometer data on
   11110011 (249): auto-send accelerometer data off
   11110100 (250): set frame numbers to auto-send data 0..127 -> 1..128
   11110101 (251): auto-send one byte
   11110110 (252): auto-send two bytes

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact

