//_cmbReading.js

var outLen;
var inputMatrix;
var outputMatrix;

post("setting outputlength to: "+100);
outputMatrix = new JitterMatrix(3, "char", 100);

function outputlength() {
	outLen = arrayfromargs(arguments)[0];
	post("setting outputlength to: "+outLen);
	outputMatrix = new JitterMatrix(3, "char", outLen);
}

function jit_matrix() {
	var matrixName = arrayfromargs(arguments)[0];
	post("matrix set: "+matrixName+"\n");
	inputMatrix = new JitterMatrix(matrixName);
	post("planecount: "+inputMatrix.planecount);
	post(", type: "+inputMatrix.type);
	post(", dim: "+inputMatrix.dim+"\n");
}

function sample_line() {
	if (inputMatrix!=null) {
	var line = arrayfromargs(arguments); // four coordinate
	var x0 = line[0];
	var y0 = line[1];
	var x1 = line[2];
	var y1 = line[3];
	//post("making line from "+x0+"/"+y0+" to "+x1+"/"+y1+"\n");
	var dx = x1-x0;
	var dy = y1-y0;
	
	for (var step=0;step<=outLen;step++) {
		var perc = step/outLen;
		var x = Math.floor(x0 + dx*perc);
		var y = Math.floor(y0 + dy*perc);
		var rgb = inputMatrix.getcell(x,y);
		if (rgb!=null) {
			outputMatrix.setcell1d(step,rgb[1],rgb[2],rgb[3]);
			//post(rgb+",");
		}
	}
	//post("\n");
	

	outlet(0, "jit_matrix", outputMatrix.name);
	}
}

	