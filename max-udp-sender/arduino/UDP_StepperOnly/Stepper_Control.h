void stepper_run(float freq) {
#ifdef DEBUG_STEPPER
  Serial.print("stepper_run: freq=");
  Serial.println(freq);
#endif

  stepper_toneRunnning = true;
  stepper_lastFreq = freq;
#ifdef STEPPER_TONE
  toneAC(STEPS_PER_REV * freq * stepperDividerFac, true);
#endif
}

void stepper_setDirectionForward(boolean b) {
#ifdef DEBUG_STEPPER
  Serial.print("stepper_setDirectionForward: forward=");
  Serial.println(b);
#endif
  if (b) {
    // Set motor direction clockwise
    digitalWrite(DIR_PIN, HIGH);
  } else {
    digitalWrite(DIR_PIN, LOW);
  }
}




String dividerSteps[] = { "nada", "Full step", "Half step", "1/4 step", "1/8 step", "1/16 step", "1/32 step", "1/32 step", "1/32 step"};
void stepper_InitDivider(int newDivider) {

  //Full step
  if (newDivider == 1) {
    stepperDivider = newDivider;
    digitalWrite(MODE_0_PIN, LOW);
    digitalWrite(MODE_1_PIN, LOW);
    digitalWrite(MODE_2_PIN, LOW);
  }
  //Half step
  else if (newDivider == 2) {
    stepperDivider = newDivider;
    digitalWrite(MODE_0_PIN, HIGH);
    digitalWrite(MODE_1_PIN, LOW);
    digitalWrite(MODE_2_PIN, LOW);
  }
  //1/4 step
  else if (newDivider == 3) {
    stepperDivider = newDivider;
    digitalWrite(MODE_0_PIN, LOW);
    digitalWrite(MODE_1_PIN, HIGH);
    digitalWrite(MODE_2_PIN, LOW);
  }
  //1/8 step
  else if (newDivider == 4) {
    stepperDivider = newDivider;
    digitalWrite(MODE_0_PIN, HIGH);
    digitalWrite(MODE_1_PIN, HIGH);
    digitalWrite(MODE_2_PIN, LOW);
  }
  //1/16 step
  else if (newDivider == 5) {
    stepperDivider = newDivider;
    digitalWrite(MODE_0_PIN, 0);
    digitalWrite(MODE_1_PIN, 0);
    digitalWrite(MODE_2_PIN, 1);
  }
  //1/32 step
  else if (newDivider == 6) {
    stepperDivider = newDivider;
    digitalWrite(MODE_0_PIN, 1);
    digitalWrite(MODE_1_PIN, 0);
    digitalWrite(MODE_2_PIN, 1);
  }
  //1/32 step
  else if (newDivider == 7) {
    stepperDivider = newDivider;
    digitalWrite(MODE_0_PIN, LOW);
    digitalWrite(MODE_1_PIN, 1);
    digitalWrite(MODE_2_PIN, 1);
  }
  //1/32 step
  else if (newDivider == 8) {
    stepperDivider = newDivider;
    digitalWrite(MODE_0_PIN, HIGH);
    digitalWrite(MODE_1_PIN, HIGH);
    digitalWrite(MODE_2_PIN, 1);
  }

  stepperDividerFac = min(32, round(pow(2, stepperDivider)) / 2);

  if (stepper_toneRunnning) {
    stepper_run(stepper_lastFreq);
  }

#ifdef DEBUG_STEPPER
  Serial.print("stepperDivider=");
  Serial.print(stepperDivider);
  Serial.print(", stepperDividerFac=");
  Serial.println(stepperDividerFac);
#endif
}


//void


//void



void stepperDemo() {

  Serial.println("Stepper Demo:");
  for (int divider = 1; divider <= 6; divider++) {
    float freq = 0.4;
    //int divider = 1;
    stepper_InitDivider(divider);
    float divFac = min(32, round(pow(2, stepperDivider)) / 2);
    Serial.print("freq=");
    Serial.print(freq);
    Serial.print("  divider=");
    Serial.print(divider);
    Serial.print("(");
    Serial.print(dividerSteps[divider]);
    Serial.print(")");
    Serial.print("  divFac=");
    Serial.println(divFac);
#ifdef STEPPER_TONE
    toneAC(STEPS_PER_REV * freq * divFac, true);
#endif
    delay(1500);
  }
  
#ifdef STEPPER_TONE
  toneAC();
#endif
  
  Serial.println("Done.");
}




void setupStepper() {
  // Setup the pins as Outputs
  Serial.println("Setting up stepper pins");
  pinMode(MODE_0_PIN, OUTPUT);
  pinMode(MODE_1_PIN, OUTPUT);
  pinMode(MODE_2_PIN, OUTPUT);
  pinMode(STEP_PIN, OUTPUT);
  pinMode(DIR_PIN, OUTPUT);
#ifdef STEPPER_DEMO
  stepperDemo();
#else
  Serial.println("Skipping Stepper Demo");
#endif

  stepper_InitDivider(STEPPER_DEFAULT_DIVIDER);
  stepper_setDirectionForward(STEPPER_DEFAULT_DIRECTION_FORWARD);
  Serial.println();
}




void stepper_stop() {
#ifdef DEBUG_STEPPER
  Serial.println("stepper_stop");
#endif
  stepper_toneRunnning = false;
#ifdef STEPPER_TONE
  toneAC();
#endif
}





