

//#define STEPPER_DEMO
#define STEPPER_TONE
//#define DEBUG_STEPPER


/*


   ---------- LIBRARY INCLUDES ----------


*/

#ifdef STEPPER_TONE
#include <toneAC.h>  /// to run the stepper in the background: https://github.com/teckel12/arduino-toneac
#endif
#include <SPI.h>         // COMMENT OUT THIS LINE FOR GEMMA OR TRINKET
#include <Ethernet.h>
#include <EthernetUdp.h>         // UDP library from: bjoern@cs.stanford.edu 12/30/2008
#include <OSCMessage.h>     //https://github.com/CNMAT/OSC




////-------------------------------------------------------------------------------------

// Enter a MAC address and IP address for your controller below.
// The IP address will be dependent on your local network:

#define MAX_MESSAGE_ITEMS 10

byte mac[] = {
  //0x00, 0xAA, 0xBB, 0xCC, 0xDE, 0x02
  0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED
};
IPAddress ip(10, 0, 0, 11); //(192, 168, 2, 2);
//IPAddress gateway(192,168,2,11);
//IPAddress dns1(192,168,2,11);
//IPAddress subnet(255,255,0,0);
IPAddress computerIP(0, 0, 0, 0);
unsigned int localPort = 8888;      // local port to listen on
unsigned int computerPort = 8889;
// buffers for receiving and sending data
//#define UDP_TX_PACKET_MAX_SIZE 2048 //increase UDP size
char packetBuffer[UDP_TX_PACKET_MAX_SIZE];  //buffer to hold incoming packet,
char  ReplyBuffer[] = "1234";       // a string to send back

// An EthernetUDP instance to let us send and receive packets over UDP
EthernetUDP Udp;



/*

   ---------- MOTOR SETTINGS ----------

*/


#define MODE_0_PIN 2
#define MODE_1_PIN 3
#define MODE_2_PIN 4
#define DIR_PIN 8
#define STEP_PIN 12 //http://domoticx.com/arduino-library-toneac/ -> must be pin 12 due to ToneAC library
#define STEPS_PER_REV 200
#define FULLSTEP_DEGREE 1.8f
#define STEPPER_DEFAULT_DIVIDER 6
#define STEPPER_DEFAULT_DIRECTION_FORWARD true
int stepperDivider = 1;
float stepperDividerFac;
boolean stepper_toneRunnning = false;
float stepper_lastFreq = 0;




/*
   ---------- TAB INCLUDES ----------
*/


#include "Stepper_Control.h"
#include "UDPParsing.h"





void setup() {
  Serial.begin(115200);

  setupStepper();

  setupEthernet();

}





void loop() {

  // if there's data available, read a packet
  int packetSize = Udp.parsePacket();
  if (packetSize) {
    Udp.read(packetBuffer, UDP_TX_PACKET_MAX_SIZE);
    computerIP = Udp.remoteIP();
#ifdef DEBUG_UDP_PARSING
    Serial.print("computerIP=");
    Serial.println(computerIP);
#endif
    //computerPort = Udp.remotePort();
    //Serial.print(computerIP);
    String asString(packetBuffer);
    udpParse(asString);
    memset(packetBuffer, 0, UDP_TX_PACKET_MAX_SIZE);
  }
}
