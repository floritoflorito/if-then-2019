/*
   SETTING UP ETHERNET - ARDUINO ON MAC
   https://superuser.com/questions/633017/using-macbook-pro-ethernet-as-a-dhcp-server
   http://www.jacquesf.com/2011/04/mac-os-x-dhcp-server/
*/

void sendUdpTestMessageBack() {
  Serial.print("Sending UDP message with timestamp back to computer on: ");
  Serial.print(computerIP);
  Serial.print(" : ");
  Serial.println(computerPort);
  Serial.print("Chat server address:");
  Serial.println(Ethernet.localIP());
  OSCMessage msg("/test/");
  msg.add((int)millis());
  Udp.beginPacket(computerIP, computerPort);
  msg.send(Udp); // send the bytes to the SLIP stream
  Udp.endPacket();
  msg.empty(); // free space occupied by message
}

void setupEthernet() {
  Serial.print("Setting up ethernet: ");
  //  Serial.print(String((char *)mac));
  Serial.print("ip=");
  Serial.print(ip);
  Serial.print(", localPort=");
  Serial.println(localPort);

  //mac, ip, dns, gateway, subnet
  Ethernet.begin(mac, ip);//, dns1, gateway, subnet);
  
  int result = Udp.begin(localPort);
  Serial.print("Udp.begin result=");
  Serial.println(result);

  Serial.print("Chat server address:");
  Serial.println(Ethernet.localIP());
  Serial.print("Remote IP: ");
  computerIP = Udp.remoteIP();
  Serial.println(computerIP);
  // start the Ethernet and UDP

  Serial.print("UDP_TX_PACKET_MAX_SIZE=");
  Serial.println(UDP_TX_PACKET_MAX_SIZE);

#ifdef DEBUG_UDP_PARSING
  Serial.println("DEBUG_UDP_PARSING ENABLED");
#endif
  Serial.println();
}




void udpParse(String string) {

  // convert string to char array 'buf
  int len = string.length();
  char buf[len];
  string.toCharArray(buf, len);

  String message[MAX_MESSAGE_ITEMS + 2];
  int index = 0;

  char* sub = strtok(buf, "_");
  message[index] = sub;
  index++;

  //  Serial.println(sub);
  while (sub != NULL) {
    sub = strtok(NULL, "_");
    message[index] = sub;
    index++;
    //    Serial.println(sub);
  }

  int dataLength = index - 2;

  char command = message[0].charAt(0);
  // Serial.print(message[0]);

#ifdef DEBUG_UDP_PARSING
  Serial.print(message[0]);
  Serial.print("  ");
  Serial.print(message[1].toInt());
  Serial.print("  ");
  Serial.print(message[2].toInt());
  Serial.print("  ");
  Serial.print(message[3].toInt());
  Serial.print("  ");
  Serial.print(message[4].toInt());
  Serial.print("  ");
  Serial.print(message[5].toInt());
  Serial.print("  ");
  Serial.print(message[6].toInt());
  Serial.print("  ");
  Serial.println(message[7].toInt());
#endif
  /// converting the string to integer
  int data_array[MAX_MESSAGE_ITEMS + 2];
  data_array[0] = 0; // For no real reason. just to work with array index 1 :D
  for (int i = 1; i <= index; i++) {
    data_array[i] = message[i].toInt();
    //    Serial.print(data_array[i]);
    //    Serial.print("  ");
  }
  //Serial.println("");
  //  if (1 > 0) {
  switch (command) {
    case 's':  stepper_InitDivider(data_array[1]); break;
    case 't':  stepper_run(data_array[1] / 1000.0f); break;
    case 'u':  stepper_stop(); break;
    case 'v':  stepper_setDirectionForward(data_array[1] == 1); break;

    case 'Z':  sendUdpTestMessageBack();
  }

  //  }

}
