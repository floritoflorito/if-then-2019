//#define BAK1
#define BAK2


#define BAK1_IP 10, 0, 0, 11
#define BAK1_MAC 0x00, 0xAA, 0xBB, 0xCC, 0xDE, 0x02
#define BAK1_SEND_PORT 8889
#define BAK1_IDENTIFIER 1

#define BAK2_IP 10, 0, 0, 12
#define BAK2_MAC 0x00, 0xAA, 0xBB, 0xCC, 0xDE, 0x03
#define BAK2_SEND_PORT 8891
#define BAK2_IDENTIFIER 2

//0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED


//#define DEBUG_UDP_PARSING


// MAKE SURE MPU_ENABLED IS NOT COMMENTED!
#define MPU_ENABLED
//#define DEBUG_MPU


//#define DEBUG_LEDCONTROL
//#define LED_DEMO


//#define STEPPER_DEMO
// Use STEPPER_TONE or STEPPER_OLDSCHOOL
//#define STEPPER_TONE
#define STEPPER_OLDSCHOOL
//#define DEBUG_STEPPER


#define BUBBLES_ENABLED
//#define DEBUG_BUBBLES
//#define BUBBLE_DEMO


/*


   ---------- LIBRARY INCLUDES ----------


*/

#ifdef STEPPER_TONE
#include <toneAC.h>  /// to run the stepper in the background: https://github.com/teckel12/arduino-toneac
#endif

#include <SPI.h>         // COMMENT OUT THIS LINE FOR GEMMA OR TRINKET
#include <Ethernet.h>
#include <EthernetUdp.h>         // UDP library from: bjoern@cs.stanford.edu 12/30/2008
#include <Adafruit_NeoPixel.h> //https://github.com/adafruit/Adafruit_NeoPixel
#include <Wire.h>
#include <MPU6050_tockn.h>  //https://github.com/Tockn/MPU6050_tockn
#include <OSCMessage.h>     //https://github.com/CNMAT/OSC

/*

   ---------- NEO PIXEL SETTINGS ----------

*/

// 0..57 0-> using 58 LEDS
#define NEOPIXEL0_PIN        5
#define NEOPIXEL0_NUMPIXELS 161 // 0..160

#define NEOPIXEL1_PIN        9
#define NEOPIXEL1_NUMPIXELS 161 // 161..321


Adafruit_NeoPixel Neopixel0(NEOPIXEL0_NUMPIXELS, NEOPIXEL0_PIN, NEO_GRBW + NEO_KHZ800);
Adafruit_NeoPixel Neopixel1(NEOPIXEL1_NUMPIXELS, NEOPIXEL1_PIN, NEO_GRBW + NEO_KHZ800);
//Adafruit_NeoPixel Neopixel0(NEOPIXEL0_NUMPIXELS, NEOPIXEL0_PIN, NEO_GRB);

Adafruit_NeoPixel *ledStrip(int totalPixelIndex) {
  if (totalPixelIndex < NEOPIXEL0_NUMPIXELS) return &Neopixel0; else return &Neopixel1;
}
int ledStripIndex(int totalPixelIndex) {
  if (totalPixelIndex < NEOPIXEL0_NUMPIXELS) {
    return totalPixelIndex;
  } else {
    return totalPixelIndex - NEOPIXEL0_NUMPIXELS;
  }
}


////-------------------------------------------------------------------------------------

// Enter a MAC address and IP address for your controller below.
// The IP address will be dependent on your local network:

#define MAX_MESSAGE_ITEMS 10

//byte mac[] = {
//  //0x00, 0xAA, 0xBB, 0xCC, 0xDE, 0x02
//  0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED
//};

#ifdef BAK1

IPAddress ip(BAK1_IP);
byte mac[] = { BAK1_MAC };
int IDENTIFIER = BAK1_IDENTIFIER;
unsigned int computerPort = BAK1_SEND_PORT;
#endif

#ifdef BAK2

IPAddress ip(BAK2_IP);
byte mac[] = { BAK2_MAC };
int IDENTIFIER = BAK2_IDENTIFIER;
unsigned int computerPort = BAK2_SEND_PORT;
#endif

//(192, 168, 2, 2);
//IPAddress gateway(192,168,2,11);
//IPAddress dns1(192,168,2,11);
//IPAddress subnet(255,255,0,0);
IPAddress computerIP(0, 0, 0, 0);
unsigned int localPort = 8888;      // local port to listen on

// buffers for receiving and sending data
//#define UDP_TX_PACKET_MAX_SIZE 2048 //increase UDP size
#define UDP_TX_PACKET_MAX_SIZE 128 // default = 24


char packetBuffer[UDP_TX_PACKET_MAX_SIZE];  //buffer to hold incoming packet,
char  ReplyBuffer[] = "1234";       // a string to send back

// An EthernetUDP instance to let us send and receive packets over UDP
EthernetUDP Udp;



/*

   ---------- MOTOR SETTINGS ----------

*/


#define MODE_0_PIN 2
#define MODE_1_PIN 3
#define MODE_2_PIN 4
#define DIR_PIN 8
#define STEP_PIN 12 //http://domoticx.com/arduino-library-toneac/ -> must be pin 12 due to ToneAC library
#define STEPS_PER_REV 200
#define FULLSTEP_DEGREE 1.8f
#define STEPPER_DEFAULT_DIVIDER 6
#define STEPPER_DEFAULT_DIRECTION_FORWARD true

int stepperDivider = 1;
float stepperDividerFac;

#ifdef STEPPER_TONE
float stepper_lastFreq = 0;
boolean stepper_toneRunnning = false;
#endif

#ifdef STEPPER_OLDSCHOOL
#define STEPPER_MINIMUM_MICROSECONDS 10
// unscaled delay time as given by user
int stepperDelayMicroseconds = 1000; // fastest=350..3000 already quite slow
// actual delay time used
// one will be set through stepperDelayMicroseconds and stepeprDivider in stepper_updateActualDelayTime with min 350
int stepperActualDelayTime = 1000;
boolean stepperContinuousTurning = false;
float stepperTargetTurnDegrees = 0;
int stepperDividerNumber = 1;
#endif

/*

   ---------- ACCELEROMETER SETTINGS ----------

*/
// On Arduino Nano: Connect 5V, GND. And SCL to Analog 5 and SDA to Analog 4
// On Arduino Mega: Conenct 5V, GND. And SCL to SCL (pin 21) and SDA to SDA (pin 20)

#ifdef MPU_ENABLED
MPU6050 mpu6050(Wire);
#endif
float deg_0 = 0;
float last_Angle_X = 0;
float Angle_X = 0;
float Vel_X = 0;
#define ACC_DATA_MODE_OFF 0
#define ACC_DATA_MODE_ANGLE 1
#define ACC_DATA_MODE_DELTA_ANGLE 2
#define ACC_DATA_MODE_ANGULAR_VELOCITY 3
unsigned long accelerometerLastNow = 999999;
int accelerometerDataMode = 0;                  // set&read with 'o', set with 'O'
int accelerometerAutomaticSendingInterval = 1000; //TODO set from UDP ->
boolean accelerometerAutomaticSending = false;  //TODO set from UDP


/*

   ---------- BUBBLE SETTINGS ----------

*/

// '1' on schematic drawing:
#define BUBBLE_PWM_PIN 6
// '2' on schematic drawing
#define BUBBLE_DIRECTION_PIN 7
#define BUBBLE_SWITCH_TIME 15000
unsigned long bubbleLastCycleTime = 0;
boolean bubble_polarity = true;
boolean bubble_enabled = false;


/*
   ---------- TAB INCLUDES ----------
*/


#include "BubbleControl.h"
#include "LEDControl.h"
#include "Stepper_Control.h"
#include "IMU.h"
#include "UDPParsing.h"





void setup() {
  Serial.begin(115200);

  Serial.print("Identifier: ");
  Serial.println(IDENTIFIER);
  Serial.println();

  setupStepper();

  setupNeoPixels();

  setupAccelerometer();

#ifdef BUBBLES_ENABLED
  setupBubbles();
#else
  Serial.println("Bubbles DISABLED");
#endif

  setupEthernet();

}





void loop() {

#ifdef BUBBLES_ENABLED
    updateBubbles();
#endif

#ifdef STEPPER_OLDSCHOOL
    stepper_oldschool_update();
#endif

#ifdef MPU_ENABLED
    if (accelerometerAutomaticSending) {
      accelerometerAutomaticSendingUpdate();
    }
#endif

    // if there's data available, read a packet
    int packetSize = Udp.parsePacket();
    if (packetSize) {
      Udp.read(packetBuffer, UDP_TX_PACKET_MAX_SIZE);
      computerIP = Udp.remoteIP();
#ifdef DEBUG_UDP_PARSING
      Serial.print("computerIP=");
      Serial.println(computerIP);
#endif
      //computerPort = Udp.remotePort();
      //Serial.print(computerIP);
      String asString(packetBuffer);
      udpParse(asString);
      memset(packetBuffer, 0, UDP_TX_PACKET_MAX_SIZE);
    }

}
