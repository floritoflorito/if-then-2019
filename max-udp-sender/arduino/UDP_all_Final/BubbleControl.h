
void setBubbleEnabled(boolean b) {
  bubble_enabled = b;
#ifdef DEBUG_BUBBLES
  Serial.print("setBubbleEnabled ");
  Serial.println(bubble_enabled);
#endif
  if (bubble_enabled) {
    digitalWrite(BUBBLE_PWM_PIN, HIGH);
  } else {
    digitalWrite(BUBBLE_PWM_PIN, LOW);
  }
}

void bubble_switchPolarity() {
  bubble_polarity = !bubble_polarity;
#ifdef DEBUG_BUBBLES
  Serial.print("Switching polarity to ");
  Serial.print(bubble_polarity);
  Serial.print(" at time ");
  Serial.println(millis());
#endif

  pinMode(BUBBLE_DIRECTION_PIN, OUTPUT);
  if (bubble_polarity) {
#ifdef DEBUG_BUBBLES
    Serial.println("HIGH");
#endif
    digitalWrite(BUBBLE_DIRECTION_PIN, HIGH);
  } else {
#ifdef DEBUG_BUBBLES
    Serial.println("LOW");
#endif
    digitalWrite(BUBBLE_DIRECTION_PIN, LOW);
  }
}

void updateBubbles() {
  if (bubble_enabled) {
    unsigned long now = millis();
    now %= BUBBLE_SWITCH_TIME;

    if (now < bubbleLastCycleTime) {
      bubble_switchPolarity();
    }

    bubbleLastCycleTime = now;
  }
}

void demoBubbles() {
  Serial.println("Enabling bubbles for 3 seconds");
  setBubbleEnabled(true);
  delay(3000);
  Serial.println("Disabling bubbles for 3 seconds");
  setBubbleEnabled(false);
  delay(3000);
  Serial.println("Enabling bubbles for 3 seconds with polarity one");
  setBubbleEnabled(true);
  delay(3000);
  Serial.println("Switching polarity and waiting 6 seconds");
  bubble_switchPolarity();
  delay(6000);
  Serial.println("Switching polarity again and waiting again 6 seconds");
  bubble_switchPolarity();
  delay(6000);
  Serial.println("Switching polarity again and waiting again 6 seconds");
  bubble_switchPolarity();
  delay(6000);
  Serial.println("Turning bubbles off");
  setBubbleEnabled(false);
  delay(1000);
}


void setupBubbles() {
  Serial.println("====================================================");
  Serial.println("Setting up BUBBLES");

  pinMode(BUBBLE_PWM_PIN, OUTPUT);
  pinMode(BUBBLE_DIRECTION_PIN, OUTPUT);
#ifdef BUBBLE_DEMO
  Serial.println("Bubble Demo!");
  demoBubbles();
#else
  Serial.println("Skipping bubble demo");
#endif
  Serial.println("====================================================");
  Serial.println();
  Serial.println();

  //setBubbleEnabled(true);
}


