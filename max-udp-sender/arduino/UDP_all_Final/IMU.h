

void setupAccelerometer() {
  //----------------
  //  delay(2000);   // make sure the water is still before caliberating the Accelerometer sensor
#ifdef MPU_ENABLED
  Serial.println("====================================================");
  Serial.print("Setting up and calibrating mpu6050 (ACCELEROMETER)");
  Wire.begin();
  mpu6050.begin();
  mpu6050.calcGyroOffsets(true);
  for (int i = 0; i < 50; i++) { // computing averged initial conditions
    delay(5);
    mpu6050.update();
    deg_0 += mpu6050.getAngleX();
  }
  deg_0 = deg_0 / 50; // this value are the initial conditions for the sesnor and we substract them later on from each measurment
  Serial.println();
  Serial.print("deg_0=");
  Serial.print(deg_0);
  Serial.println(". Done.");
  Serial.println("====================================================");
  Serial.println();
#else
  Serial.println("MPU NOT ENABLED");
#endif
  Serial.println();
}



void Get_me_angle(float Angle) {
#ifdef DEBUG_MPU
  Serial.print("Sending Angle = ");
  Serial.print(Angle_X);
  Serial.print(" to ");
  Serial.print(computerIP);
  Serial.print(":");
  Serial.println(computerPort);
#endif
  OSCMessage msg("/angle/");
  msg.add(Angle_X);
  Udp.beginPacket(computerIP, computerPort);
  msg.send(Udp); // send the bytes to the SLIP stream
  Udp.endPacket();
  msg.empty(); // free space occupied by message

  //  Udp.beginPacket(computerIP, computerPort); //Initialize packet send
  //  Udp.write("ang="); //Send the Pressure data
  //  //dtostrf(floatvar, StringLengthIncDecimalPoint, numVarsAfterDecimal, charbuf);
  //  char result[11];
  //  dtostrf(Angle_X, 8, 3, result);
  //  Udp.write(result);
  //  //  Udp.write(ReplyBuffer);
  //  Udp.endPacket(); //End the packet

}

void Get_me_change_of_angle(float delta_Angle ) {
#ifdef DEBUG_MPU
  Serial.print("Sending change Of Angle =  ");
  Serial.print(delta_Angle);
  Serial.print(" to ");
  Serial.print(computerIP);
  Serial.print(":");
  Serial.println(computerPort);
#endif
  OSCMessage msg("/dangle/");
  msg.add(delta_Angle);
  Udp.beginPacket(computerIP, computerPort);
  msg.send(Udp); // send the bytes to the SLIP stream
  Udp.endPacket();
  msg.empty(); // free space occupied by message
}

void Get_me_angular_velocity(float Vel_X) {
#ifdef DEBUG_MPU
  Serial.print("Sending Angular_velocity = ");
  Serial.print(Vel_X);
  Serial.print(" to ");
  Serial.print(computerIP);
  Serial.print(":");
  Serial.println(computerPort);
#endif
  OSCMessage msg("/angvel/");
  msg.add(Vel_X);
  Udp.beginPacket(computerIP, computerPort);
  msg.send(Udp); // send the bytes to the SLIP stream
  Udp.endPacket();
  msg.empty(); // free space occupied by message
}




void IMU_readings(int mode) {
  //delay(5);
#ifdef MPU_ENABLED
#ifdef DEBUG_MPU
  Serial.print("IMU_readings mode=");
  Serial.println(mode);
#endif
  mpu6050.update();
  Angle_X = mpu6050.getAngleX() - deg_0;
  Vel_X = mpu6050.getGyroX();
  float delta_Angle = Angle_X - last_Angle_X;
  switch (mode) {
    case 1: Get_me_angle(Angle_X);  break;
    case 2: Get_me_change_of_angle(delta_Angle);  break;
    case 3: Get_me_angular_velocity(Vel_X);  break;
  }
  last_Angle_X = Angle_X;
#endif

}


void setAccelerometerAutomaticSending(bool value) {
#ifdef DEBUG_MPU
  Serial.print("setAccelerometerAutomaticSending ");
  Serial.println(value);
#endif
  accelerometerAutomaticSending = value;
  accelerometerLastNow=999999;
}
void setAccelerometerAutomaticSendingInterval(int value) {
#ifdef DEBUG_MPU
  Serial.print("setAccelerometerAutomaticSendingInterval ");
  Serial.println(value);
#endif
  accelerometerAutomaticSendingInterval = max(5, value);
}

void accelerometerAutomaticSendingUpdate() {

  unsigned long now = millis();
  now %= accelerometerAutomaticSendingInterval;
  if (now < accelerometerLastNow) {
    if (accelerometerDataMode>0)
      IMU_readings(accelerometerDataMode);
  }
  accelerometerLastNow = now;
}




