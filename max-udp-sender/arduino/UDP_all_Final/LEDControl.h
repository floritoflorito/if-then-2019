





void ledDemo() {
#ifdef DEBUG_LEDCONTROL
  Serial.println("ledDemo");
#endif

  int totalSequenceTime = 1000;


  uint32_t fiveColours[] = { Neopixel0.Color(255, 0, 0, 0), Neopixel0.Color(0, 255, 0, 0), Neopixel0.Color(0, 0, 255, 0), Neopixel0.Color(255, 255, 255, 0), Neopixel0.Color(0, 0, 0, 255) };
  String colorNames[] = { "red", "green", "blue", "rgb", "white" };

  Serial.println("TESTING NEOPIXEL0:");
  for (int i = 0; i < 5; i++) {
    Neopixel0.fill( Neopixel0.Color(0, 0, 0, 255), 0, NEOPIXEL0_NUMPIXELS);
    Neopixel0.show();
    delay(500);
    Neopixel0.clear();
    Neopixel0.show();
    delay(500);
  }

  Serial.println("TESTING NEOPIXEL1:");
  for (int i = 0; i < 5; i++) {
    Neopixel1.fill( Neopixel1.Color(0, 0, 0, 255), 0, NEOPIXEL1_NUMPIXELS);
    Neopixel1.show();
    delay(500);
    Neopixel1.clear();
    Neopixel1.show();
    delay(500);
  }


  //  singlePixelTime = totalSequenceTime / NEOPIXEL0_NUMPIXELS;
  //  for (int c = 0; c < 5; c++) {
  //    Serial.print("Testing colour: ");
  //    Serial.println(colorNames[c]);
  //    for (int i = 0; i < NEOPIXEL0_NUMPIXELS; i++) {
  //      Serial.print("neopixel0 on: ");
  //      Serial.print(i);
  //      Serial.print("/");
  //      Serial.println(NEOPIXEL0_NUMPIXELS);
  //      Neopixel0.setPixelColor(i, fiveColours[c]);
  //      Neopixel0.show();
  //      delay(singlePixelTime);
  //    }
  //  }
  //
  //  for (int i = 0; i < NEOPIXEL0_NUMPIXELS; i++) {
  //    Serial.print("neopixel0 off: ");
  //    Serial.print(i);
  //    Serial.print("/");
  //    Serial.println(NEOPIXEL0_NUMPIXELS);
  //    Neopixel0.setPixelColor(i, 0, 0, 0, 0);
  //    Neopixel0.show();
  //    delay(singlePixelTime);
  //  }


  //  int singlePixelTime = totalSequenceTime / NEOPIXEL1_NUMPIXELS;
  //  for (int c = 0; c < 5; c++) {
  //    Serial.print("Testing colour: ");
  //    Serial.println(colorNames[c]);
  //    for (int i = 0; i < NEOPIXEL1_NUMPIXELS; i++) {
  //      Serial.print("neopixel1 on: ");
  //      Serial.print(i);
  //      Serial.print("/");
  //      Serial.println(NEOPIXEL1_NUMPIXELS);
  //      Neopixel1.setPixelColor(i, fiveColours[c]);
  //      Neopixel1.show();
  //      delay(singlePixelTime);
  //    }
  //  }
  //
  //  for (int i = 0; i < NEOPIXEL1_NUMPIXELS; i++) {
  //    Serial.print("neopixel1 off: ");
  //    Serial.print(i);
  //    Serial.print("/");
  //    Serial.println(NEOPIXEL1_NUMPIXELS);
  //    Neopixel1.setPixelColor(i, 0, 0, 0, 0);
  //    Neopixel1.show();
  //    delay(singlePixelTime);
  //  }



}


void setupNeoPixels() {
  Serial.println("====================================================");
  Serial.println("Setting up NEOPIXELS");
  //  Neopixel0.clear(); // Set all pixel colors to 'off'
  Neopixel0.begin(); // INITIALIZE NeoPixel strip object (REQUIRED)
  Neopixel0.show();
  Neopixel1.begin(); // INITIALIZE NeoPixel strip object (REQUIRED)
  Neopixel1.show();
#ifdef LED_DEMO
  ledDemo();
#else
  Serial.println("Skipping LED demo");
#endif
  Serial.println("====================================================");
  Serial.println();
  Serial.println();

}





void allLedsOff() {
#ifdef DEBUG_LEDCONTROL
  Serial.println("allLedsOff");
#endif
  Neopixel0.clear();
  Neopixel0.show();
  Neopixel1.clear();
  Neopixel1.show();
}

void allLedsOnRGB() {
  Neopixel0.fill( Neopixel0.Color(255, 255, 255, 0), 0, NEOPIXEL0_NUMPIXELS);
  Neopixel0.show();
  Neopixel1.fill( Neopixel1.Color(255, 255, 255, 0), 0, NEOPIXEL1_NUMPIXELS);
  Neopixel1.show();

#ifdef DEBUG_LEDCONTROL
  Serial.println("allLedsOnRGB");
#endif
}

void allLedsOnWhite() {
  Neopixel0.fill( Neopixel0.Color(0, 0, 0, 255), 0, NEOPIXEL0_NUMPIXELS);
  Neopixel0.show();
  Neopixel1.fill( Neopixel1.Color(0, 0, 0, 255), 0, NEOPIXEL1_NUMPIXELS);
  Neopixel1.show();

#ifdef DEBUG_LEDCONTROL
  Serial.println("allLedsOnWhite");
#endif
}


void allLedsBrightRGB(int data[]) {
#ifdef DEBUG_LEDCONTROL
  Serial.print("allLedsBrightRGB: brightness=");
#endif
  int Bright = data[1];
  byte bright = Bright;
#ifdef DEBUG_LEDCONTROL
  Serial.println(bright);
#endif
  Neopixel0.fill( Neopixel0.Color(bright, bright, bright, 0), 0, NEOPIXEL0_NUMPIXELS);
  Neopixel0.show();
  Neopixel1.fill( Neopixel1.Color(bright, bright, bright, 0), 0, NEOPIXEL1_NUMPIXELS);
  Neopixel1.show();

}

void allLedsBrightWhite(int data[]) {
  int bright = data[1];
#ifdef DEBUG_LEDCONTROL
  Serial.print("allLedsBrightWhite: brightness=");
  Serial.println(bright);
#endif
  Neopixel0.fill( Neopixel0.Color(0, 0, 0, bright), 0, NEOPIXEL0_NUMPIXELS);
  Neopixel0.show();
  Neopixel1.fill( Neopixel1.Color(0, 0, 0, bright), 0, NEOPIXEL1_NUMPIXELS);
  Neopixel1.show();

}

void allLedsRGB(int data[]) {
  int red = data[1];
  int green = data[2];
  int blue = data[3];
  int bright = 0;
#ifdef DEBUG_LEDCONTROL
  Serial.print("allLedsRGB: ");
  Serial.print("r=");
  Serial.print(red);
  Serial.print(", g=");
  Serial.print(green);
  Serial.print(", b=");
  Serial.println(blue);
#endif
  Neopixel0.fill( Neopixel0.Color(red, green, blue, bright), 0, NEOPIXEL0_NUMPIXELS);
  Neopixel0.show();
  Neopixel1.fill( Neopixel1.Color(red, green, blue, bright), 0, NEOPIXEL1_NUMPIXELS);
  Neopixel1.show();

  //  Neopixel1.fill( Neopixel1.Color(red, green, blue, bright), 0, NEOPIXEL1_NUMPIXELS);
  //  Neopixel1.show();
}



void allLedsRGBW(int data[]) {
  int red = data[1];
  int green = data[2];
  int blue = data[3];
  int white = data[4];
#ifdef DEBUG_LEDCONTROL
  Serial.print("allLedsRGBW: ");
  Serial.print("r=");
  Serial.print(red);
  Serial.print(", g=");
  Serial.print(green);
  Serial.print(", b=");
  Serial.print(blue);
  Serial.print(", w=");
  Serial.println(white);
#endif
  Neopixel0.fill( Neopixel0.Color(red, green, blue, white), 0, NEOPIXEL0_NUMPIXELS);
  Neopixel0.show();
  Neopixel1.fill( Neopixel1.Color(red, green, blue, white), 0, NEOPIXEL1_NUMPIXELS);
  Neopixel1.show();
}






void ledOff(int data[]) {
  int ledIndex = data[1];
#ifdef DEBUG_LEDCONTROL
  Serial.print("ledOff: ledIndex=");
  Serial.println(ledIndex);
#endif
  Adafruit_NeoPixel *pointerToStrip = ledStrip(ledIndex);
  pointerToStrip->setPixelColor(ledStripIndex(ledIndex), pointerToStrip->Color(0, 0, 0, 0));
  pointerToStrip->show();
  //  Neopixel0.setPixelColor(ledIndex, Neopixel0.Color(0, 0, 0, 0));
  //  Neopixel0.show();
}

void ledOnRGB(int data[]) {
  int ledIndex = data[1];
#ifdef DEBUG_LEDCONTROL
  Serial.print("ledOnRGB: ledIndex=");
  Serial.println(ledIndex);
#endif
  Adafruit_NeoPixel *pointerToStrip = ledStrip(ledIndex);
  pointerToStrip->setPixelColor(ledStripIndex(ledIndex), pointerToStrip->Color(255, 255, 255, 0));
  pointerToStrip->show();
  //  Neopixel0.setPixelColor(ledIndex, Neopixel0.Color(255, 255, 255, 0));
  //  Neopixel0.show();
}

void ledOnWhite(int data[], boolean showIt) {
  int ledIndex = data[1];
#ifdef DEBUG_LEDCONTROL
  Serial.print("ledOnWhite: ledIndex=");
  Serial.println(ledIndex);
#endif
  Adafruit_NeoPixel *pointerToStrip = ledStrip(ledIndex);
  pointerToStrip->setPixelColor(ledStripIndex(ledIndex), pointerToStrip->Color(0, 0, 0, 255));
  if (showIt)
    pointerToStrip->show();
  //  Neopixel0.setPixelColor(ledIndex, Neopixel0.Color(0, 0, 0, 255));
  //  Neopixel0.show();
}


void ledFadeRGB(int data[]) {
  int ledIndex = data[1];
  int brightness = data[2];
#ifdef DEBUG_LEDCONTROL
  Serial.print("ledFadeRGB: ledIndex=");
  Serial.print(ledIndex);
  Serial.print(", brightness=");
  Serial.println(brightness);
#endif
  Adafruit_NeoPixel *pointerToStrip = ledStrip(ledIndex);
  pointerToStrip->setPixelColor(ledStripIndex(ledIndex), pointerToStrip->Color(brightness, brightness, brightness, 0));
  pointerToStrip->show();
  //  Neopixel0.setPixelColor(ledIndex, Neopixel0.Color(brightness, brightness, brightness, 0));
  //  Neopixel0.show();
}


void ledFadeWhite(int data[], boolean showIt) {
  int ledIndex = data[1];
  int brightness = data[2];
#ifdef DEBUG_LEDCONTROL
  Serial.print("ledFadeWhite: ledIndex=");
  Serial.print(ledIndex);
  Serial.print(", brightness=");
  Serial.println(brightness);
#endif
  Adafruit_NeoPixel *pointerToStrip = ledStrip(ledIndex);
  pointerToStrip->setPixelColor(ledStripIndex(ledIndex), pointerToStrip->Color(0, 0, 0, brightness));
  if (showIt)
    pointerToStrip->show();
  //  Neopixel0.setPixelColor(ledIndex, Neopixel0.Color(0, 0, 0, brightness));
  //  Neopixel0.setBrightness(brightness);
  //  Neopixel0.show();
}

void ledRGB(int data[]) {
  int ledIndex = data[1];
  int red = data[2];
  int green = data[3];
  int blue = data[4];
#ifdef DEBUG_LEDCONTROL
  Serial.print("ledRGB: ledIndex=");
  Serial.print(ledIndex);
  Serial.print(", r=");
  Serial.print(red);
  Serial.print(", g=");
  Serial.print(green);
  Serial.print(", b=");
  Serial.println(blue);
#endif
  Adafruit_NeoPixel *pointerToStrip = ledStrip(ledIndex);
  pointerToStrip->setPixelColor(ledStripIndex(ledIndex), pointerToStrip->Color(red, green, blue, 0));
  pointerToStrip->show();
  //  Neopixel0.setPixelColor(ledIndex, Neopixel0.Color(red, green, blue, 0));
  //  Neopixel0.show();
}

void ledRGBW(int data[], boolean showIt) {
  int ledIndex = data[1];
  int red = data[2];
  int green = data[3];
  int blue = data[4];
  int white = data[5];
  float intensity = 1;
  //  Neopixel0.setPixelColor(ledIndex, Neopixel0.Color(red, green, blue, white));
  //  if (showIt) Neopixel0.show();   // Send the updated pixel colors to the hardware.

  Adafruit_NeoPixel *pointerToStrip = ledStrip(ledIndex);
  pointerToStrip->setPixelColor(ledStripIndex(ledIndex), pointerToStrip->Color(red, green, blue, white));
  if (showIt) pointerToStrip->show();

#ifdef DEBUG_LEDCONTROL
  Serial.print("ledRGBW: ledIndex=");
  Serial.print(ledIndex);
  Serial.print(", r=");
  Serial.print(red);
  Serial.print(", g=");
  Serial.print(green);
  Serial.print(", b=");
  Serial.print(blue);
  Serial.print(", w=");
  Serial.println(white);
#endif
}

void ledsShow() {
  Neopixel0.show();
  Neopixel1.show();
}
void leds0Show() {
  Neopixel0.show();
}
void leds1Show() {
  Neopixel1.show();
}


//void ledsArrayBrightWhite(int count, int data[]) {
//#ifdef DEBUG_LEDCONTROL
//  Serial.print("ledsArrayBrightWhite: count=");
//  Serial.println(count);
//#endif
//  for (int i = 1; i <= count; i++) {
//    Neopixel0.setPixelColor(i - 1, Neopixel0.Color(0, 0, 0, data[i]));
//  }
//  Neopixel0.show();
//}
