/*


   STEPPER: TONE SPECIFIC COMMANDS:


*/

#define MAX_LOOPING_TIME_MS 10

#ifdef STEPPER_TONE

void stepper_run(float freq) {
# ifdef DEBUG_STEPPER
  Serial.print("stepper_run: freq=");
  Serial.println(freq);
# endif
  stepper_toneRunnning = true;
  stepper_lastFreq = freq;
  toneAC(STEPS_PER_REV * freq * stepperDividerFac, true);
}

void stepper_stop() {
# ifdef DEBUG_STEPPER
  Serial.println("stepper_stop");
# endif
  stepper_toneRunnning = false;
  toneAC();
}


#endif


/*


   STEPPER: OLDSCHOOL SPECIFIC COMMANDS


*/

#ifdef STEPPER_OLDSCHOOL

void stepper_oldschool_updateActualDelayTime() {
# ifdef DEBUG_STEPPER
  Serial.println("stepper_oldschool_updateActualDelayTime");
# endif
  stepperActualDelayTime = max(STEPPER_MINIMUM_MICROSECONDS, stepperDelayMicroseconds);// / stepperDividerNumber);
}

void stepper_oldschool_reset() {
# ifdef DEBUG_STEPPER
  Serial.println("stepper_oldschool_reset");
# endif
  stepperContinuousTurning = false;
  stepperTargetTurnDegrees = 0;
  stepper_oldschool_updateActualDelayTime();
}


void stepper_oldschool_setDelayTime(int microS) {
# ifdef DEBUG_STEPPER
  Serial.print("stepper_oldschool_setDelayTime ");
  Serial.println(microS, DEC);
# endif
  stepperDelayMicroseconds = max(STEPPER_MINIMUM_MICROSECONDS, microS);
  stepper_oldschool_updateActualDelayTime();
}

void stepper_oldschool_setTargetCentiDegrees(int centiDegrees) {
  float mdeg = centiDegrees / 100.0f;
# ifdef DEBUG_STEPPER
  Serial.print("stepper_oldschool_setTargetCentiDegrees ");
  Serial.print(centiDegrees);
  Serial.print(" -> ");
  Serial.println(mdeg);
# endif
  stepperTargetTurnDegrees = mdeg;//(incomingByte + 1) * 2.8125f;
  stepperContinuousTurning = false;
}

void stepper_oldschool_setContinuousTurning(bool b) {
#ifdef DEBUG_STEPPER
  Serial.print("stepper_oldschool_setContinuousTurning ");
  Serial.println(b);
#endif
  stepperContinuousTurning = b;
}



void stepper_oldschool_update() {
  if (stepperContinuousTurning) {
    digitalWrite(STEP_PIN, HIGH);
    delayMicroseconds(stepperActualDelayTime);
    digitalWrite(STEP_PIN, LOW);
    delayMicroseconds(stepperActualDelayTime);
  }
  else if (stepperTargetTurnDegrees != 0) {
    int steps = (int)(stepperTargetTurnDegrees / FULLSTEP_DEGREE * stepperDividerNumber);
    // Spin motor one rotation slowly

    long totalTimeForAllSteps = (long)steps * (long)stepperActualDelayTime * 2;
    long totalTimeForAllStepsMilliseconds = totalTimeForAllSteps / 1000;

    // totalTimeMS = steps * stepperdelay * 2 / 1000
    // now we want MAX_LOOPING_TIME_MS for totalTimeMS -> how many steps?
    // MAX_LOOPING_TIME_MS = steps * stepperdelay * 2 / 1000
    //-> steps = MAX_LOOPING_TIME_MS * 1000 / 2 / stepperdelay


    //    float totalTimeSeconds = totalTimeForAllSteps/1000000.0f;

    if (totalTimeForAllStepsMilliseconds > MAX_LOOPING_TIME_MS) {
      int newSteps = (int)(MAX_LOOPING_TIME_MS * 1000.0f / 2.0f / stepperActualDelayTime);
      // now calc how many degrees remaining.
      //      int stepsNotDone = steps - newSteps;
      float stepperTargetTurnDegreesDone = (float)newSteps / stepperDividerNumber * FULLSTEP_DEGREE;
      float origStepperTargetTurnDegrees = stepperTargetTurnDegrees;
      stepperTargetTurnDegrees -= stepperTargetTurnDegreesDone;

//      OSCMessage msg("/debug/");
//      msg.add("targetdeg (orig)");
//      msg.add(origStepperTargetTurnDegrees);
//      msg.add("steps");
//      msg.add(steps);
//      msg.add("time_would_be");
//      msg.add(totalTimeForAllStepsMilliseconds);
//      msg.add("actual steps");
//      msg.add(newSteps);
//      msg.add("remaining degs" );
//      msg.add(stepperTargetTurnDegrees);
//      Udp.beginPacket(computerIP, computerPort);
//      msg.send(Udp); // send the bytes to the SLIP stream
//      Udp.endPacket();
//      msg.empty(); // free space occupied by message

      steps = newSteps;
    }

    else {
//
//      OSCMessage msg("/debug/");
//      msg.add("steps");
//      msg.add(steps);
//      Udp.beginPacket(computerIP, computerPort);
//      msg.send(Udp); // send the bytes to the SLIP stream
//      Udp.endPacket();
//      msg.empty(); // free space occupied by message
      
      stepperTargetTurnDegrees = 0;
    }

    for (int x = 0; x < steps; x++) {
      digitalWrite(STEP_PIN, HIGH);
      delayMicroseconds(stepperActualDelayTime);
      digitalWrite(STEP_PIN, LOW);
      delayMicroseconds(stepperActualDelayTime);
    }

  }
}


#endif


/*


   STEPPER: GENERAL (TONE & OLDSCHOOL)


*/



void stepper_setDirectionForward(boolean b) {
#ifdef DEBUG_STEPPER
  Serial.print("stepper_setDirectionForward: forward=");
  Serial.println(b);
#endif
  if (b) {
    // Set motor direction clockwise
    digitalWrite(DIR_PIN, HIGH);
  } else {
    digitalWrite(DIR_PIN, LOW);
  }
}




String dividerSteps[] = { "nada", "Full step", "Half step", "1/4 step", "1/8 step", "1/16 step", "1/32 step", "1/32 step", "1/32 step"};
void stepper_InitDivider(int newDivider) {

  //Full step
  if (newDivider == 1) {
    stepperDivider = newDivider;
    stepperDividerNumber = 1;
    digitalWrite(MODE_0_PIN, LOW);
    digitalWrite(MODE_1_PIN, LOW);
    digitalWrite(MODE_2_PIN, LOW);
  }
  //Half step
  else if (newDivider == 2) {
    stepperDivider = newDivider;
    stepperDividerNumber = 2;
    digitalWrite(MODE_0_PIN, HIGH);
    digitalWrite(MODE_1_PIN, LOW);
    digitalWrite(MODE_2_PIN, LOW);
  }
  //1/4 step
  else if (newDivider == 3) {
    stepperDivider = newDivider;
    stepperDividerNumber = 4;
    digitalWrite(MODE_0_PIN, LOW);
    digitalWrite(MODE_1_PIN, HIGH);
    digitalWrite(MODE_2_PIN, LOW);
  }
  //1/8 step
  else if (newDivider == 4) {
    stepperDivider = newDivider;
    stepperDividerNumber = 8;
    digitalWrite(MODE_0_PIN, HIGH);
    digitalWrite(MODE_1_PIN, HIGH);
    digitalWrite(MODE_2_PIN, LOW);
  }
  //1/16 step
  else if (newDivider == 5) {
    stepperDivider = newDivider;
    stepperDividerNumber = 16;
    digitalWrite(MODE_0_PIN, 0);
    digitalWrite(MODE_1_PIN, 0);
    digitalWrite(MODE_2_PIN, 1);
  }
  //1/32 step
  else if (newDivider == 6) {
    stepperDivider = newDivider;
    stepperDividerNumber = 32;
    digitalWrite(MODE_0_PIN, 1);
    digitalWrite(MODE_1_PIN, 0);
    digitalWrite(MODE_2_PIN, 1);
  }
  //1/32 step
  else if (newDivider == 7) {
    stepperDivider = newDivider;
    stepperDividerNumber = 32;
    digitalWrite(MODE_0_PIN, LOW);
    digitalWrite(MODE_1_PIN, 1);
    digitalWrite(MODE_2_PIN, 1);
  }
  //1/32 step
  else if (newDivider == 8) {
    stepperDivider = newDivider;
    stepperDividerNumber = 32;
    digitalWrite(MODE_0_PIN, HIGH);
    digitalWrite(MODE_1_PIN, HIGH);
    digitalWrite(MODE_2_PIN, 1);
  }

  stepperDividerFac = min(32, round(pow(2, stepperDivider)) / 2);

#ifdef STEPPER_TONE
  if (stepper_toneRunnning) {
    stepper_run(stepper_lastFreq);
  }
#endif

#ifdef STEPPER_OLDSCHOOL
  stepper_oldschool_updateActualDelayTime();
#endif

#ifdef DEBUG_STEPPER
  Serial.print("stepperDivider=");
  Serial.print(stepperDivider);
  Serial.print(", stepperDividerNumber=");
  Serial.print(stepperDividerNumber);
  Serial.print(", stepperDividerFac=");
  Serial.println(stepperDividerFac);
#endif
}


//void


//void





#ifdef STEPPER_TONE
void stepperDemoTone() {
  Serial.println("Stepper Demo Tone:");
  for (int divider = 1; divider <= 6; divider++) {
    float freq = 0.4;
    //int divider = 1;
    stepper_InitDivider(divider);
    float divFac = min(32, round(pow(2, stepperDivider)) / 2);
    Serial.print("freq=");
    Serial.print(freq);
    Serial.print("  divider=");
    Serial.print(divider);
    Serial.print("(");
    Serial.print(dividerSteps[divider]);
    Serial.print(")");
    Serial.print("  divFac=");
    Serial.println(divFac);

    toneAC(STEPS_PER_REV * freq * divFac, true);

    delay(1500);
  }

  toneAC();


  Serial.println("Done.");
}
#endif



void setupStepper() {
  // Setup the pins as Outputs
  Serial.println("====================================================");
  Serial.println("Setting up STEPPER/MOTOR");
  pinMode(MODE_0_PIN, OUTPUT);
  pinMode(MODE_1_PIN, OUTPUT);
  pinMode(MODE_2_PIN, OUTPUT);
  pinMode(STEP_PIN, OUTPUT);
  pinMode(DIR_PIN, OUTPUT);

#ifdef STEPPER_OLDSCHOOL
  Serial.println("OLDSCHOOl mode activated");
  stepper_oldschool_reset();
#endif

#ifdef STEPPER_TONE
  Serial.println("TONE mode activated");
#endif

#ifdef STEPPER_DEMO
  stepperDemoTone();
#else
  Serial.println("Skipping Stepper Demo Tone (TODO: Stepper demo oldschool?)");
#endif

  stepper_InitDivider(STEPPER_DEFAULT_DIVIDER);
  stepper_setDirectionForward(STEPPER_DEFAULT_DIRECTION_FORWARD);
  Serial.println("====================================================");
  Serial.println();
  Serial.println();
}








