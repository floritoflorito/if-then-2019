/*
   SETTING UP ETHERNET - ARDUINO ON MAC
   https://superuser.com/questions/633017/using-macbook-pro-ethernet-as-a-dhcp-server
   http://www.jacquesf.com/2011/04/mac-os-x-dhcp-server/
*/


void sendUdpTestMessageBack() {
//  int ti = (int)millis();
  float seconds = millis()/1000.0f;
  
  Serial.print("Sending UDP test message seconds=");
  Serial.println(seconds);
//  Serial.print(ti);
//  Serial.print(": ");
//  Serial.print(computerIP);
//  Serial.print(":");
//  Serial.println(computerPort);
//  Serial.print("Chat server address:");
//  Serial.println(Ethernet.localIP());
  OSCMessage msg("/test/");
//  msg.add(ti);
  msg.add(IDENTIFIER);
  Udp.beginPacket(computerIP, computerPort);
  msg.send(Udp); // send the bytes to the SLIP stream
  Udp.endPacket();
  msg.empty(); // free space occupied by message
}

void setupEthernet() {
  Serial.println("====================================================");
  Serial.println("Setting up ETHERNET: ");
  Serial.print("mac=");
  for (int i = 0; i < 6; i++) {
    Serial.print(mac[i], HEX);
    Serial.print(" ");
  }
  Serial.println();
  //  Serial.print(String((char *)mac));
  Serial.print("ip=");
  Serial.println(ip);
  Serial.print("localPort=");
  Serial.println(localPort);

  Serial.print("Sending messages on port ");
  Serial.println(computerPort);

  //mac, ip, dns, gateway, subnet
  Ethernet.begin(mac, ip);//, dns1, gateway, subnet);

  int result = Udp.begin(localPort);

  Serial.print("Udp.begin result=");
  Serial.println(result);

  Serial.print("Chat server address:");
  Serial.println(Ethernet.localIP());
  Serial.print("Remote IP: ");
  computerIP = Udp.remoteIP();

  Serial.println(computerIP);
  // start the Ethernet and UDP

  Serial.print("UDP_TX_PACKET_MAX_SIZE=");
  Serial.println(UDP_TX_PACKET_MAX_SIZE);

#ifdef DEBUG_UDP_PARSING
  Serial.println("DEBUG_UDP_PARSING ENABLED");
#endif
  Serial.println("====================================================");
  Serial.println();
  Serial.println();
}




void udpParse(String string) {

  // convert string to char array 'buf
  int len = string.length();
  char buf[len];
  string.toCharArray(buf, len);

  String message[MAX_MESSAGE_ITEMS + 2];
  int index = 0;

  char* sub = strtok(buf, "_");
  message[index] = sub;
  index++;

  //  Serial.println(sub);
  while (sub != NULL) {
    sub = strtok(NULL, "_");
    message[index] = sub;
    index++;
    //    Serial.println(sub);
  }

  int dataLength = index - 2;

  char command = message[0].charAt(0);
  // Serial.print(message[0]);

#ifdef DEBUG_UDP_PARSING
  Serial.print(message[0]);
  Serial.print("  ");
  Serial.print(message[1].toInt());
  Serial.print("  ");
  Serial.print(message[2].toInt());
  Serial.print("  ");
  Serial.print(message[3].toInt());
  Serial.print("  ");
  Serial.print(message[4].toInt());
  Serial.print("  ");
  Serial.print(message[5].toInt());
  Serial.print("  ");
  Serial.print(message[6].toInt());
  Serial.print("  ");
  Serial.println(message[7].toInt());
#endif
  /// converting the string to integer
  int data_array[MAX_MESSAGE_ITEMS + 2];
  data_array[0] = 0; // For no real reason. just to work with array index 1 :D
  for (int i = 1; i <= index; i++) {
    data_array[i] = message[i].toInt();
    //    Serial.print(data_array[i]);
    //    Serial.print("  ");
  }
  //Serial.println("");
  //  if (1 > 0) {
  switch (command) {
    // LEDS:
    case 'a': if (dataLength == 0) allLedsOff();  break;
    case 'b': if (dataLength == 0) allLedsOnRGB();  break;
    case 'c': if (dataLength == 0) allLedsOnWhite();  break;
    case 'd': if (dataLength == 1) allLedsBrightRGB(data_array);  break;
    case 'e': if (dataLength == 1) allLedsBrightWhite(data_array);  break;
    case 'f': if (dataLength == 3) allLedsRGB(data_array);  break;
    case 'g': if (dataLength == 4) allLedsRGBW(data_array);  break;
    case 'h': if (dataLength == 1) ledOff(data_array);  break;
    case 'i': if (dataLength == 1) ledOnRGB(data_array);  break;
    case 'j': if (dataLength == 1) ledOnWhite(data_array, true);  break;
    case 'J': if (dataLength == 1) ledOnWhite(data_array, false); break;
    case 'k': if (dataLength == 2) ledFadeRGB(data_array);  break;
    case 'l': if (dataLength == 2) ledFadeWhite(data_array, true);  break;
    case 'L': if (dataLength == 2) ledFadeWhite(data_array, false);  break;
    case 'm': if (dataLength == 4) ledRGB(data_array);  break;
    case 'n': if (dataLength == 5) ledRGBW(data_array, true);  break;
    case 'N': if (dataLength == 5) ledRGBW(data_array, false);  break;
    case 'S':
      if (dataLength == 0) {
        ledsShow();
      }
      else if (dataLength == 1) {
        if (data_array[1] == 0) leds0Show();
        else if (data_array[1] == 1) leds1Show();
      }
      break;


    // ACCELEROMETER:

    case 'o':
      if (dataLength == 1) {
        accelerometerDataMode = data_array[1];
        if (accelerometerDataMode > 0)
          IMU_readings(data_array[1]);
      }
      break;
    case 'O':
      if (dataLength == 1) {
        accelerometerDataMode = data_array[1];
      }
      break;
    case 'p':
      if (dataLength == 1) {
        setAccelerometerAutomaticSending(data_array[1] == 1);
      }
      break;
    case 'P':
      if (dataLength == 1) {
        setAccelerometerAutomaticSendingInterval(data_array[1]);
      }
      break;


      // STEPPER
#ifdef STEPPER_TONE
    case 't':  stepper_run(data_array[1] / 1000.0f); break;
    case 'u':  stepper_stop(); break;
#endif

#ifdef STEPPER_OLDSCHOOL
    case 'T':  stepper_oldschool_setContinuousTurning(data_array  [1] == 1); break;
    case 'U':  stepper_oldschool_setDelayTime(data_array[1]); break;
    case 'V':  stepper_oldschool_setTargetCentiDegrees(data_array[1]); break;
#endif

    case 's':  stepper_InitDivider(data_array[1]); break;
    case 'v':  stepper_setDirectionForward(data_array[1] == 1); break;

    // BUBBLES
    case 'w':  setBubbleEnabled(data_array[1] == 1); break;
    case 'W':  bubble_switchPolarity(); break;

    // TEST:
    case 'Z':  sendUdpTestMessageBack();
  }

  //  }

}
