void allLedsOff() {
  Serial.println("allLedsOff");
  for (int ledIndex = 0; ledIndex < Neo_NUMPIXELS_warm; ledIndex++) {
    Neo_pixels_warm.setPixelColor(ledIndex, Neo_pixels_warm.Color(0,0,0,0));
   // Neo_pixels_cold.setPixelColor(ledIndex, Neo_pixels_cold.Color(0,0,0,0));
   // Dot_Star.setPixelColor(ledIndex, 0,0,0); // 'On' pixel at head
  }
//  Neo_pixels_warm.show();   // Send the updated pixel colors to the hardware.
//  Neo_pixels_cold.show();   // Send the updated pixel colors to the hardware.
//  Dot_Star.show();                     // Refresh Dot_Star
 // Neo_pixels_warm.fill( Neo_pixels_warm.Color(0,0,0,0),1,Neo_NUMPIXELS_warm);
  Neo_pixels_warm.show();
}

void allLedsOnRGB() {
   Neo_pixels_warm.fill( Neo_pixels_warm.Color(255,255,255,255),1,Neo_NUMPIXELS_warm);
  Neo_pixels_warm.show();
  Serial.println("allLedsOnRGB");
}

void allLedsOnWhite() {
 Neo_pixels_warm.fill(Neo_pixels_warm.Color(0,0,0,255),1,Neo_NUMPIXELS_warm);
   Neo_pixels_warm.show();
  Serial.println("allLedsOnWhite");
}

void allLedsBrightRGB(String data[]) {
  Serial.print("allLedsBrightRGB: brightness=");
  int Bright = data[1].toInt();
  byte bright = Bright;
  Serial.println(bright);
  Neo_pixels_warm.fill( Neo_pixels_warm.Color(bright,bright,bright,bright),1,Neo_NUMPIXELS_warm);
  Neo_pixels_warm.show();
}

void allLedsBrightWhite(String data[]) {
  Serial.print("allLedsBrightWhite: brightness=");
  int bright = data[1].toInt();
  Serial.println(bright);
    Neo_pixels_warm.fill( Neo_pixels_warm.Color(0,0,0,bright),1,Neo_NUMPIXELS_warm);
  Neo_pixels_warm.show();
}

void allLedsRGB(String data[]) {
  Serial.print("allLedsRGB: ");
  int red = data[1].toInt();
  int green = data[2].toInt();
  int blue = data[3].toInt();
  int bright =0;
Neo_pixels_warm.fill( Neo_pixels_warm.Color(red,green,blue,bright),1,Neo_NUMPIXELS_warm);
Neo_pixels_warm.show();
  Serial.print("r=");
  Serial.print(red);
  Serial.print(", g=");
  Serial.print(green);
  Serial.print(", b=");
  Serial.println(blue);
}

void allLedsRGBW(String data[]) {
  Serial.print("allLedsRGBW: ");
  int red = data[1].toInt();
  int green = data[2].toInt();
  int blue = data[3].toInt();
  int white = data[4].toInt();
  Neo_pixels_warm.fill( Neo_pixels_warm.Color(red,green,blue,white),1,Neo_NUMPIXELS_warm);
  Neo_pixels_warm.show();
  Serial.print("r=");
  Serial.print(red);
  Serial.print(", g=");
  Serial.print(green);
  Serial.print(", b=");
  Serial.print(blue);
  Serial.print(", w=");
  Serial.println(white);
}





void ledOff(String data[]) {
  Serial.print("ledOff: ledIndex=");
  int ledIndex = data[1].toInt();
  Serial.println(ledIndex);
  Neo_pixels_warm.setPixelColor(ledIndex, Neo_pixels_warm.Color(0,0,0,0));
  Neo_pixels_warm.show();
}

void ledOnRGB(String data[]) {
  Serial.print("ledOnRGB: ledIndex=");
  int ledIndex = data[1].toInt();
  Serial.println(ledIndex);
    Neo_pixels_warm.setPixelColor(ledIndex, Neo_pixels_warm.Color(random(10,255),random(10,255),random(10,255),0));
    Neo_pixels_warm.show();
}

void ledOnWhite(String data[]) {
  Serial.print("ledOnWhite: ledIndex=");
  int ledIndex = data[1].toInt();
  Serial.println(ledIndex);
 Neo_pixels_warm.setPixelColor(ledIndex, Neo_pixels_warm.Color(0,0,0,255));
  Neo_pixels_warm.show();
}


void ledFadeRGB(String data[]) {
  Serial.print("ledFadeRGB: ledIndex=");
  int ledIndex = data[1].toInt();
  Serial.print(ledIndex);
  int brightness = data[2].toInt();
  Serial.print(", brightness=");
  Serial.println(brightness);
   Neo_pixels_warm.setPixelColor(ledIndex, Neo_pixels_warm.Color(0,0,0,brightness));
    Neo_pixels_warm.setBrightness(brightness);
  Neo_pixels_warm.show();
}


void ledFadeWhite(String data[]) {
  Serial.print("ledFadeWhite: ledIndex=");
  int ledIndex = data[1].toInt();
  Serial.print(ledIndex);
  int brightness = data[2].toInt();
  Serial.print(", brightness=");
  Serial.println(brightness);
   Neo_pixels_warm.setPixelColor(ledIndex, Neo_pixels_warm.Color(0,0,0,brightness));
    Neo_pixels_warm.setBrightness(brightness);
}

void ledRGB(String data[]) {
  int ledIndex = data[1].toInt();
  int red = data[2].toInt();
  int green = data[3].toInt();
  int blue = data[4].toInt();
  Serial.print("ledRGB: ledIndex=");
  Serial.print(ledIndex);
  Serial.print(", r=");
  Serial.print(red);
  Serial.print(", g=");
  Serial.print(green);
  Serial.print(", b=");
  Serial.println(blue);
     Neo_pixels_warm.setPixelColor(ledIndex, Neo_pixels_warm.Color(red,green,blue,0));
    Neo_pixels_warm.show();
}

void ledRGBW(String data[]) {
  int ledIndex = data[1].toInt();
  int red = data[2].toInt();
  int green = data[3].toInt();
  int blue = data[4].toInt();
  int white = data[5].toInt();
  float intensity = 1;
  Neo_pixels_warm.setPixelColor(ledIndex, Neo_pixels_warm.Color(intensity * red, intensity * green, intensity * blue, intensity * white));
//  Neo_pixels_cold.setPixelColor(ledIndex, Neo_pixels_cold.Color(intensity * red, intensity * green, intensity * blue,  intensity * white));
//  Dot_Star.setPixelColor(ledIndex, intensity * red, intensity * green, intensity * blue); // 'On' pixel at head
  Neo_pixels_warm.show();   // Send the updated pixel colors to the hardware.
//  Neo_pixels_cold.show();   // Send the updated pixel colors to the hardware.
//  Dot_Star.show();                     // Refresh Dot_Star
  Serial.print("ledRGBW: ledIndex=");
  Serial.print(ledIndex);
  Serial.print(", r=");
  Serial.print(red);
  Serial.print(", g=");
  Serial.print(green);
  Serial.print(", b=");
  Serial.print(blue);
  Serial.print(", w=");
  Serial.println(white);
}
