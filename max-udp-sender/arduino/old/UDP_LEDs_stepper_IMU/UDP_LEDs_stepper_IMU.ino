/*
   SETTING UP ETHERNET - ARDUINO ON MAC
   https://superuser.com/questions/633017/using-macbook-pro-ethernet-as-a-dhcp-server
   http://www.jacquesf.com/2011/04/mac-os-x-dhcp-server/
*/

/*
  UDPSendReceiveString:
  This sketch receives UDP message strings, prints them to the serial port
  and sends an "acknowledge" string back to the sender

  A Processing sketch is included at the end of file that can be used to send
  and received messages for testing with a computer.

  created 21 Aug 2010\
  by Michael Margolis

  This code is in the public domain.
*/
///////////////

#include <Adafruit_DotStar.h>
#include <SPI.h>         // COMMENT OUT THIS LINE FOR GEMMA OR TRINKET
#include <Adafruit_NeoPixel.h>

#include <Wire.h>
#include <MPU6050_tockn.h>
MPU6050 mpu6050(Wire);
#include "stepper.h"
//--------------------------------------

// Which pin on the Arduino is connected to the NeoPixels?
#define Neo_PIN        6 
// How many NeoPixels are attached to the Arduino?
#define Neo_NUMPIXELS_warm 200 // Popular NeoPixel ring size
#define Neo_NUMPIXELS_cold 164 // Popular NeoPixel ring size

// When setting up the NeoPixel library, we tell it how many pixels,
// and which pin to use to send signals. Note that for older NeoPixel
// strips you might need to change the third parameter -- see the
// strandtest example for more information on possible values.
Adafruit_NeoPixel Neo_pixels_warm(Neo_NUMPIXELS_warm, Neo_PIN, NEO_GRBW + NEO_KHZ800);

Adafruit_NeoPixel Neo_pixels_cold(Neo_NUMPIXELS_cold, 7, NEO_GRBW + NEO_KHZ800);
//-------------------------------------------------------------------------------------
#define Dot_NUMPIXELS 164 // Number of LEDs in Dot_Star
// Here's how to control the LEDs from any two pins:
#define Dot_DATAPIN    4
#define Dot_CLOCKPIN   5
//Adafruit_DotStar Dot_Star(Dot_NUMPIXELS, Dot_DATAPIN, Dot_CLOCKPIN, DOTSTAR_BRG);
//////////////
/////////////////
//#include <SPI.h>         // needed for Arduino versions later than 0018
#include <Ethernet.h>
#include <EthernetUdp.h>         // UDP library from: bjoern@cs.stanford.edu 12/30/2008
#include "LEDControl.h"
#include "UDPParsing.h"
//////////////////

// Enter a MAC address and IP address for your controller below.
// The IP address will be dependent on your local network:
byte mac[] = {
  0x00, 0xAA, 0xBB, 0xCC, 0xDE, 0x02
  //0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED
};
IPAddress ip(192, 168, 1, 65);

unsigned int localPort = 8888;      // local port to listen on

// buffers for receiving and sending data
char packetBuffer[UDP_TX_PACKET_MAX_SIZE];  //buffer to hold incoming packet,
char  ReplyBuffer[] = "acknowledged";       // a string to send back

// An EthernetUDP instance to let us send and receive packets over UDP
EthernetUDP Udp;

void setup() {
  Serial.begin(9600);
  Wire.begin();
  mpu6050.begin();
  mpu6050.calcGyroOffsets(true);
  // Setup the pins as Outputs
  pinMode(MODE_0_PIN, OUTPUT);
  pinMode(MODE_1_PIN, OUTPUT);
  pinMode(STEP_PIN, OUTPUT);
  pinMode(DIR_PIN, OUTPUT);


  //  stepper_reset();
  stepperContinuousTurning = false;
  stepperTargetTurnDegrees = 0;
  stepper_InitDivider(2);
  stepper_setDirectionForward(true);
  stepper_updateActualDelayTime();

  Neo_pixels_warm.clear(); // Set all pixel colors to 'off'
  Neo_pixels_warm.begin(); // INITIALIZE NeoPixel strip object (REQUIRED)
  Neo_pixels_cold.clear(); // Set all pixel colors to 'off'
  Neo_pixels_cold.begin(); // INITIALIZE NeoPixel strip object (REQUIRED)
//  Dot_Star.begin(); // Initialize pins for output
//  Dot_Star.show();  // Turn all LEDs off ASAP
  ////////
  // start the Ethernet and UDP:
  Ethernet.begin(mac,ip);
  Udp.begin(localPort);

  Serial.print("Chat server address:");
  Serial.println(Ethernet.localIP());
}

void loop() {
  // if there's data available, read a packet
  int packetSize = Udp.parsePacket();
  if (packetSize) {
    Udp.read(packetBuffer, UDP_TX_PACKET_MAX_SIZE);
    String asString(packetBuffer);
    udpParse(asString);
     motor_modes();
    memset(packetBuffer,0,UDP_TX_PACKET_MAX_SIZE);
  }
//  else {
//      Neo_pixels_warm.clear(); 
//    Neo_pixels_warm.show();
//  }
//
  
}

