//byte currentCommand = -1;
//byte currentData[7];
//int currentDataIndex;

void udpParse(String string) {

  int len = string.length();
  char buf[len];
  string.toCharArray(buf, len);

  String message[8];
  int index = 0;

  char* sub = strtok(buf, "_");
  message[index] = sub;
  index++;

//  Serial.println(sub);
  while (sub != NULL) {
    sub = strtok(NULL, "_");
    message[index] = sub;
    index++;
//    Serial.println(sub);
  }

  int dataLength = index-2;

  char command = message[0].charAt(0);

  

  switch (command) {
    case 'a': if (dataLength == 0) allLedsOff();  break;
    case 'b': if (dataLength == 0) allLedsOnRGB();  break;
    case 'c': if (dataLength == 0) allLedsOnWhite();  break;
    case 'd': if (dataLength == 1) allLedsBrightRGB(message);  break;
    case 'e': if (dataLength == 1) allLedsBrightWhite(message);  break;
    case 'f': if (dataLength == 3) allLedsRGB(message);  break;
    case 'g': if (dataLength == 4) allLedsRGBW(message);  break;
    
    case 'h': if (dataLength == 1) ledOff(message);  break;
    case 'i': if (dataLength == 1) ledOnRGB(message);  break;
    case 'j': if (dataLength == 1) ledOnWhite(message);  break;
    case 'k': if (dataLength == 2) ledFadeRGB(message);  break;
    case 'l': if (dataLength == 2) ledFadeWhite(message);  break;
    case 'm': if (dataLength == 4) ledRGB(message);  break;
    case 'n': if (dataLength == 5) ledRGBW(message);  break;
  }


}

