#define ID 2
//Define pin connections & motor's steps per revolution
// Motor steps per rotation
#define STEPS_PER_REV 200
#define FULLSTEP_DEGREE 1.8f

#define STEPPER_MINIMUM_MICROSECONDS 350

int stepperDivider = 1;

// unscaled delay time as given by user
int stepperDelayMicroseconds = 400; // fastest=350..3000 already quite slow

// actual delay time used
// one will be set through stepperDelayMicroseconds and stepeprDivider in stepper_updateActualDelayTime with min 350
int stepperActualDelayTime = 200;

boolean stepperForward = true;

boolean stepperContinuousTurning = false;
float stepperTargetTurnDegrees = 0;



const int stepsPerRevolution = 200;

//// ACCELEROMETER
//const int MPU_addr = 0x68;
//int16_t AcX, AcY, AcZ, Tmp, GyX, GyY, GyZ;
//int minVal = 265;
//int maxVal = 402;
double degX = 0;
double Vel_degX = 0;
double lastDegX = 0;
int deg0 = 0;
//boolean accelerator_autoSendData = false;
//int accelerator_autoSendDataFrameCounter = 0;
//int accelerator_autoSendDataFrames = 5;
//int accelerator_autoSendBytes = 1;

long timer1 = 0;
long timer2 = 0;
// STEPPER STUFF
#define MODE_0_PIN 2
#define MODE_1_PIN 3
#define DIR_PIN 8
#define STEP_PIN 9
int rotations = random(5, 10);
int Steps_m1 = rotations * 900;
///////////////////////

/* MODE2  MODE1  MODE0    DIVIDER
   0      0      0        full step (2-phase excitation)
   0      0      1        half step (1-2 phase excitation)
   0      1      0        1/4 step (W1-2 phase excitation)
   0      1      1        8 microsteps / step
*/
 
void stepper_updateActualDelayTime() {
  stepperActualDelayTime = max(STEPPER_MINIMUM_MICROSECONDS, stepperDelayMicroseconds / stepperDivider);
}

void stepper_InitDivider(int newDivider) {
  if (newDivider == 1) {
    stepperDivider = newDivider;
    digitalWrite(MODE_0_PIN, LOW);
    digitalWrite(MODE_1_PIN, LOW);
    stepper_updateActualDelayTime();
  }
  else if (newDivider == 2) {
    stepperDivider = newDivider;
    digitalWrite(MODE_0_PIN, HIGH);
    digitalWrite(MODE_1_PIN, LOW);
//     digitalWrite(MODE_0_PIN, LOW);
//    digitalWrite(MODE_1_PIN, HIGH);
    stepper_updateActualDelayTime();
  }
  else if (newDivider == 4) {
    stepperDivider = newDivider;
    digitalWrite(MODE_0_PIN, LOW);
    digitalWrite(MODE_1_PIN, HIGH);
    stepper_updateActualDelayTime();
  }
  else if (newDivider == 8) {
    stepperDivider = newDivider;
    digitalWrite(MODE_0_PIN, HIGH);
    digitalWrite(MODE_1_PIN, HIGH);
    stepper_updateActualDelayTime();
  }

}

void stepper_setDirectionForward(boolean b) {
  if (b) {
    // Set motor direction clockwise
    digitalWrite(DIR_PIN, HIGH);
  } else {
    digitalWrite(DIR_PIN, LOW);
  }
} 
void stepper_reset() {
  stepperContinuousTurning = false;
  stepperTargetTurnDegrees = 0;
  stepper_InitDivider(4);
  stepper_setDirectionForward(true);
  stepper_updateActualDelayTime();
}





void motor_modes(){
   stepperDelayMicroseconds = 900; // fastest=350..3000 already quite slow
  stepper_updateActualDelayTime();
  stepper_setDirectionForward(true);
  Serial.println(" mode: 1");
 stepper_InitDivider(2);
  // stepss=stepss;
  for (int x = 0; x < (Steps_m1); x++) {
    digitalWrite(STEP_PIN, HIGH);
    delayMicroseconds(stepperActualDelayTime);     //(1+mpu6050.getGyroY()));
    digitalWrite(STEP_PIN, LOW);
    delayMicroseconds(stepperActualDelayTime);

  }
  mpu6050.update();
  float y = mpu6050.getAngleX();
  float vel_y = mpu6050.getGyroX();
  int threshold = 1;
  float stepss = 50 * (y - lastDegX);
  lastDegX = y;

  if ((millis() - timer1) > 7000 && abs (vel_y < threshold)) {
    stepperDelayMicroseconds = 350; // fastest=350..3000 already quite slow
    stepper_updateActualDelayTime();
    Steps_m1 = 3000;
 stepper_InitDivider(2);

    Serial.println(" mode: 2");
    for (int x = 0; x < (Steps_m1); x++) {
      digitalWrite(STEP_PIN, HIGH);
      delayMicroseconds(stepperActualDelayTime);     //(1+mpu6050.getGyroY()));
      digitalWrite(STEP_PIN, LOW);
      delayMicroseconds(stepperActualDelayTime);
    }
   // delay(3000);

    stepper_setDirectionForward(false);
    for (int x = 0; x < (Steps_m1); x++) {
      digitalWrite(STEP_PIN, HIGH);
      delayMicroseconds(stepperActualDelayTime);     //(1+mpu6050.getGyroY()));
      digitalWrite(STEP_PIN, LOW);
      delayMicroseconds(stepperActualDelayTime);
    }
   // delay(3000);
    stepper_setDirectionForward(true);

    timer1 = millis();
  }
  delay(5);
  mpu6050.update();
  y = mpu6050.getAngleX();
  vel_y = mpu6050.getGyroX();
  threshold = 1;
  stepss = 50 * (y - lastDegX);
  lastDegX = y;
  if ((millis() - timer2) > 7000 && abs(vel_y < threshold)) {
    stepperDelayMicroseconds = 400; // fastest=350..3000 already quite slow
    stepper_updateActualDelayTime();
    Steps_m1 = 800;
    stepper_InitDivider(4);
    Serial.println(" mode: 3");
    for (int i = 0; i < 30; i++){
      for (int x = 0; x < (Steps_m1); x++) {
        digitalWrite(STEP_PIN, HIGH);
        delayMicroseconds(stepperActualDelayTime);     //(1+mpu6050.getGyroY()));
        digitalWrite(STEP_PIN, LOW);
        delayMicroseconds(stepperActualDelayTime);
      }
    delay(10);
    stepper_setDirectionForward(false);
    for (int x = 0; x < (Steps_m1); x++) {
      digitalWrite(STEP_PIN, HIGH);
      delayMicroseconds(stepperActualDelayTime);     //(1+mpu6050.getGyroY()));
      digitalWrite(STEP_PIN, LOW);
      delayMicroseconds(stepperActualDelayTime);
    }
    delay(10);
    stepper_setDirectionForward(true);
  }
  Steps_m1 = 900 * rotations;
  timer2 = millis();

   }

// delay(3000);
 Serial.println(" mode: 4");
   stepperDelayMicroseconds = 1200; // fastest=350..3000 already quite slow
  stepper_updateActualDelayTime();
  stepper_setDirectionForward(true);

  Serial.println(" mode: 4");
  stepper_InitDivider(2);
for (int i = 0; i < 1000; i++) {
  delay(5);
  mpu6050.update();
  y = mpu6050.getAngleX();
  vel_y = mpu6050.getGyroX();
  threshold = 1;
  stepss = 50 * (y - lastDegX);
  lastDegX = y;
  if (vel_y < threshold) stepper_setDirectionForward(false);
  for (int x = 0; x < abs(round(stepss)); x++) {
    digitalWrite(STEP_PIN, HIGH);
    delayMicroseconds(stepperActualDelayTime);     //(1+mpu6050.getGyroY()));
    digitalWrite(STEP_PIN, LOW);
    delayMicroseconds(stepperActualDelayTime);
  }
}
//delay(1000);
}


//------------------------------------------
void my_Stepper(){
  int dirPin,stepPin;
  
  // Set motor direction clockwise
  digitalWrite(dirPin, HIGH);
//digitalWrite(dirPin, LOW);
  // Spin motor slowly
  for(int x = 0; x < stepsPerRevolution; x++)
  {
    digitalWrite(stepPin, HIGH);
    delayMicroseconds(2000);
    digitalWrite(stepPin, LOW);
    delayMicroseconds(2000);
  }
  delay(1000); // Wait a second
  
  // Set motor direction counterclockwise
//  digitalWrite(dirPin, LOW);

  // Spin motor quickly
  for(int x = 0; x < stepsPerRevolution; x++)
  {
    digitalWrite(stepPin, HIGH);
    delayMicroseconds(1000);
    digitalWrite(stepPin, LOW);
    delayMicroseconds(1000);
  }
  delay(1000); // Wait a second
}


