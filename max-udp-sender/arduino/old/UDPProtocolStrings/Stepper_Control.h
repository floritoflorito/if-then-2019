void stepper_updateActualDelayTime() {
  stepperActualDelayTime = max(STEPPER_MINIMUM_MICROSECONDS, stepperDelayMicroseconds / stepperDivider);
}

void stepper_InitDivider(int newDivider) {
  if (newDivider == 1) {
    stepperDivider = newDivider;
    digitalWrite(MODE_0_PIN, LOW);
    digitalWrite(MODE_1_PIN, LOW);
    digitalWrite(MODE_2_PIN, LOW);

    stepper_updateActualDelayTime();
  }
  else if (newDivider == 2) {
    stepperDivider = newDivider;
    digitalWrite(MODE_0_PIN, HIGH);
    digitalWrite(MODE_1_PIN, LOW);
    digitalWrite(MODE_2_PIN, LOW);
    stepper_updateActualDelayTime();
  }
  else if (newDivider == 3) {
    stepperDivider = newDivider;
    digitalWrite(MODE_0_PIN, LOW);
    digitalWrite(MODE_1_PIN, HIGH);
    digitalWrite(MODE_2_PIN, LOW);
    stepper_updateActualDelayTime();
  }
  else if (newDivider == 4) {
    stepperDivider = newDivider;
    digitalWrite(MODE_0_PIN, HIGH);
    digitalWrite(MODE_1_PIN, HIGH);
    digitalWrite(MODE_2_PIN, LOW);
    stepper_updateActualDelayTime();
  }

  else if (newDivider == 5) {
    stepperDivider = newDivider;
    digitalWrite(MODE_0_PIN, 0);
    digitalWrite(MODE_1_PIN, 0);
    digitalWrite(MODE_2_PIN, 1);
    stepper_updateActualDelayTime();
  }
  else if (newDivider == 6) {
    stepperDivider = newDivider;
    digitalWrite(MODE_0_PIN, 1);
    digitalWrite(MODE_1_PIN, 0);
    digitalWrite(MODE_2_PIN, 1);
    stepper_updateActualDelayTime();
  }

  else if (newDivider == 7) {
    stepperDivider = newDivider;
    digitalWrite(MODE_0_PIN, LOW);
    digitalWrite(MODE_1_PIN, 1);
    digitalWrite(MODE_2_PIN, 1);
    stepper_updateActualDelayTime();
  }
  else if (newDivider == 8) {
    stepperDivider = newDivider;
    digitalWrite(MODE_0_PIN, HIGH);
    digitalWrite(MODE_1_PIN, HIGH);
    digitalWrite(MODE_2_PIN, 1);
    stepper_updateActualDelayTime();
  }
}


void stepper_setDirectionForward(boolean b) {
  if (b) {
    // Set motor direction clockwise
    digitalWrite(DIR_PIN, HIGH);
  } else {
    digitalWrite(DIR_PIN, LOW);
  }
}




void stepper_reset() {
  stepperContinuousTurning = false;
  stepperTargetTurnDegrees = 0;
  stepper_InitDivider(1);
  stepper_setDirectionForward(true);
  stepper_updateActualDelayTime();
}




void stepper_motor(int data[]) {
//  int divider = data[2].toInt();
//  int micro_delay = map(data[1].toInt(),0,9,350,2000);
//  int Direction = data[3].toInt();
//  int Steps = map(data[4].toInt(),0,9,0,200);
 int divider = data[1];
  int micro_delay =data[2];
  int Direction = data[3];
  int Steps = map(data[4],0,9,0,200);
//  
//long tt=millis();
// for(int i=1; i <= Neo_NUMPIXELS_warm;i++){
//    Neo_pixels_warm.setPixelColor(i, Neo_pixels_warm.Color(random(0,255),random(0,255),random(0,255),random(0,255)));
// }
//   // Neo_pixels_warm.fill( Neo_pixels_warm.Color(random(0,255),random(0,255),random(0,255),random(0,255)),1,Neo_NUMPIXELS_warm);
//Neo_pixels_warm.show();
//  Serial.print(millis()-tt);
//  Serial.print(" ");
//   stepperContinuousTurning = false;
delay(75);
  stepperTargetTurnDegrees = Steps;
 
  stepper_InitDivider(divider);
  if (Direction==0)   { stepper_setDirectionForward(false);}
  else   {stepper_setDirectionForward(true); }
// int steps = (int)(stepperTargetTurnDegrees / FULLSTEP_DEGREE * stepperDivider);
int steps =Steps;
stepperDelayMicroseconds=micro_delay;
stepper_updateActualDelayTime();
int freq = round(1/(pow(10,-6)*2*stepperActualDelayTime));
int Duration= round(steps*(pow(10,-3)*2*stepperActualDelayTime));
   Serial.print(freq);
  Serial.print(" ");
   Serial.print(Duration);
  Serial.print(" ");
   Serial.print(steps);
  Serial.print(" ");
  Serial.print("Stepper: divider=");
  Serial.print(divider);
  Serial.print(", micro_delay=");
  Serial.print(micro_delay);
  Serial.print(", Dir=");
  Serial.print(Direction);
  Serial.print(", Steps=");
  Serial.println(Steps);
 if (divider >0){
  if (Steps ==0)  tone(STEP_PIN,freq);
  else  tone(STEP_PIN,freq ,Duration);

//  
//  Serial.print("Stepper: divider=");
//  Serial.print(divider);
//  Serial.print(", micro_delay=");
//  Serial.print(micro_delay);
//  Serial.print(", Dir=");
//  if (Direction==0)  Serial.print(" Right");
//  else  Serial.print(" Left"); 
//  Serial.print(", Steps=");
//  Serial.println(Steps);
  }
  else {
      stepper_InitDivider(2);
       Duration= round(2*(pow(10,-3)*2*stepperActualDelayTime));
      tone(STEP_PIN,freq ,Duration);
  
     //Serial.println("Motor is off");
  }

}
