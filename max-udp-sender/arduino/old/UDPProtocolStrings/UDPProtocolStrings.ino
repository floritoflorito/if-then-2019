/*
   SETTING UP ETHERNET - ARDUINO ON MAC
   https://superuser.com/questions/633017/using-macbook-pro-ethernet-as-a-dhcp-server
   http://www.jacquesf.com/2011/04/mac-os-x-dhcp-server/
*/

/*
  UDPSendReceiveString:
  This sketch receives UDP message strings, prints them to the serial port
  and sends an "acknowledge" string back to the sender

  A Processing sketch is included at the end of file that can be used to send
  and received messages for testing with a computer.

  created 21 Aug 2010\
  by Michael Margolis

  This code is in the public domain.
*/
///////////////

#include <Adafruit_DotStar.h>
#include <SPI.h>         // COMMENT OUT THIS LINE FOR GEMMA OR TRINKET
#include <Adafruit_NeoPixel.h>


// Which pin on the Arduino is connected to the NeoPixels?
#define Neo_PIN        6 
// How many NeoPixels are attached to the Arduino?
#define Neo_NUMPIXELS_warm 200 // Popular NeoPixel ring size
#define Neo_NUMPIXELS_cold 164 // Popular NeoPixel ring size

// When setting up the NeoPixel library, we tell it how many pixels,
// and which pin to use to send signals. Note that for older NeoPixel
// strips you might need to change the third parameter -- see the
// strandtest example for more information on possible values.
Adafruit_NeoPixel Neo_pixels_warm(Neo_NUMPIXELS_warm, Neo_PIN, NEO_GRBW + NEO_KHZ800);

//Adafruit_NeoPixel Neo_pixels_cold(Neo_NUMPIXELS_cold, 7, NEO_GRBW + NEO_KHZ800);
////-------------------------------------------------------------------------------------
#define Dot_NUMPIXELS 164 // Number of LEDs in Dot_Star
// Here's how to control the LEDs from any two pins:
#define Dot_DATAPIN    4
#define Dot_CLOCKPIN   5
//Adafruit_DotStar Dot_Star(Dot_NUMPIXELS, Dot_DATAPIN, Dot_CLOCKPIN, DOTSTAR_BRG);
//////////////
/////////////////
//#include <SPI.h>         // needed for Arduino versions later than 0018
#include <Ethernet.h>
#include <EthernetUdp.h>         // UDP library from: bjoern@cs.stanford.edu 12/30/2008
//#include <Wire.h>
//#include <MPU6050_tockn.h>
//MPU6050 mpu6050(Wire);

//////////////////

// Enter a MAC address and IP address for your controller below.
// The IP address will be dependent on your local network:
byte mac[] = {
  0x00, 0xAA, 0xBB, 0xCC, 0xDE, 0x02
  //0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED
};
IPAddress ip(192, 168, 1, 65);

unsigned int localPort = 8888;      // local port to listen on

// buffers for receiving and sending data
char packetBuffer[UDP_TX_PACKET_MAX_SIZE];  //buffer to hold incoming packet,
char  ReplyBuffer[] = "acknowledged";       // a string to send back

// An EthernetUDP instance to let us send and receive packets over UDP
EthernetUDP Udp;

//



//----------MOTOR---------------------


// Connections to A4988
#define ID 2

// STEPPER STUFF
#define MODE_0_PIN 2
#define MODE_1_PIN 3
#define MODE_2_PIN 4
#define DIR_PIN 8
#define STEP_PIN 9

// Motor steps per rotation
#define STEPS_PER_REV 200
#define FULLSTEP_DEGREE 1.8f

#define STEPPER_MINIMUM_MICROSECONDS 350

int stepperDivider = 1;

// unscaled delay time as given by user
int stepperDelayMicroseconds = 1000; // fastest=350..3000 already quite slow

// actual delay time used
// one will be set through stepperDelayMicroseconds and stepeprDivider in stepper_updateActualDelayTime with min 350
int stepperActualDelayTime = 1000;

boolean stepperForward = true;

boolean stepperContinuousTurning = false;
float stepperTargetTurnDegrees = 0;










//----------Accelerometer---------------------


#include "LEDControl.h"
#include "Stepper_Control.h"
#include "UDPParsing.h"
void setup() {
  // Setup the pins as Outputs
  pinMode(MODE_0_PIN, OUTPUT);
  pinMode(MODE_1_PIN, OUTPUT);
  pinMode(STEP_PIN, OUTPUT);
  pinMode(DIR_PIN, OUTPUT);
  
  Neo_pixels_warm.clear(); // Set all pixel colors to 'off'
  Neo_pixels_warm.begin(); // INITIALIZE NeoPixel strip object (REQUIRED)
//  Neo_pixels_cold.clear(); // Set all pixel colors to 'off'
 // Neo_pixels_cold.begin(); // INITIALIZE NeoPixel strip object (REQUIRED)
//  Dot_Star.begin(); // Initialize pins for output
//  Dot_Star.show();  // Turn all LEDs off ASAP
  ////////
  // start the Ethernet and UDP:
  Ethernet.begin(mac,ip);
  Udp.begin(localPort);

  Serial.begin(115200);
  Serial.print("Chat server address:");
  Serial.println(Ethernet.localIP());
}

void loop() {
  // if there's data available, read a packet
  int packetSize = Udp.parsePacket();
  if (packetSize) {
    Udp.read(packetBuffer, UDP_TX_PACKET_MAX_SIZE);
    String asString(packetBuffer);
    udpParse(asString);
   // memset(packetBuffer,0,UDP_TX_PACKET_MAX_SIZE);
  }
  //int steps;
//if (1<0){
//
//  if (stepperTargetTurnDegrees==0) {
//    digitalWrite(STEP_PIN, HIGH);
//    delayMicroseconds(stepperActualDelayTime);
//    digitalWrite(STEP_PIN, LOW);
//    delayMicroseconds(stepperActualDelayTime);
//  }
//
//
//  else if (stepperTargetTurnDegrees != 0) {
//
//    // Spin motor one rotation slowly
//    for (int x = 0; x < steps; x++) {
//      digitalWrite(STEP_PIN, HIGH);
//      delayMicroseconds(stepperActualDelayTime);
//      digitalWrite(STEP_PIN, LOW);
//      delayMicroseconds(stepperActualDelayTime);
//    }
//    steps=0;
    //stepperTargetTurnDegrees = 0;

  }
  
//}

  
//  else {
//      Neo_pixels_warm.clear(); 
//    Neo_pixels_warm.show();
//  }
 
  
//}
