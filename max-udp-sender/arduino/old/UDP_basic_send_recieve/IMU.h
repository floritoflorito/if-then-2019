void Get_me_angle(float Angle) {
  Serial.print("Angle = ");
  Serial.println(Angle_X);
        Udp.beginPacket(Udp.remoteIP(), Udp.remotePort()); //Initialize packet send
        Udp.print(Angle_X); //Send the Pressure data
        Udp.endPacket(); //End the packet
        
}

void Get_me_change_of_angle(float delta_Angle ) {
  Serial.print("Change Of Angle =  ");
  Serial.println(delta_Angle);
  last_Angle_X=Angle_X;
}

void Get_me_angular_velocity(float Vel_X) {
   Serial.print("Angular_velocity = ");
  Serial.println(Vel_X);
}

void IMU_readings(int data[]){
delay(5);
   mpu6050.update();
   Angle_X = mpu6050.getAngleX()-deg_0;
 Vel_X = mpu6050.getGyroX();
   float delta_Angle=Angle_X-last_Angle_X;
   switch (data[1]) {
    case 1: Get_me_angle(Angle_X);  break;
    case 2: Get_me_change_of_angle(delta_Angle);  break;
    case 3: Get_me_angular_velocity(Vel_X);  break;    
   }
  last_Angle_X=Angle_X; 
  
}
