/*
   SETTING UP ETHERNET - ARDUINO ON MAC
   https://superuser.com/questions/633017/using-macbook-pro-ethernet-as-a-dhcp-server
   http://www.jacquesf.com/2011/04/mac-os-x-dhcp-server/
check this to connect to python
   http://www.toptechboy.com/tutorial/python-with-arduino-lesson-17-sending-and-receiving-data-over-ethernet/
*/
//  there is a python file to run this script with.
/*
  UDPSendReceiveString:
  This sketch receives UDP message strings, prints them to the serial port
  and sends an "acknowledge" string back to the sender

  A Processing sketch is included at the end of file that can be used to send
  and received messages for testing with a computer.

  created 21 Aug 2010\
  by Michael Margolis

  This code is in the public domain.
*/
///////////////

#include <toneAC.h>  /// to run the stepper in the background 
#include <SPI.h>         // COMMENT OUT THIS LINE FOR GEMMA OR TRINKET
#include <Ethernet.h>
#include <EthernetUdp.h>         // UDP library from: bjoern@cs.stanford.edu 12/30/2008
#include <Adafruit_NeoPixel.h>
#include <Wire.h> 
#include <MPU6050_tockn.h>
MPU6050 mpu6050(Wire);
//--------------------------------------
// Not related to this example just to compile without errors

// Which pin on the Arduino is connected to the NeoPixels?
#define Neo_PIN_1        6 
#define Neo_PIN_2        5

// How many NeoPixels are attached to the Arduino?
#define Neo_NUMPIXELS_warm1 180 // Popular NeoPixel ring size
#define Neo_NUMPIXELS_warm2 180 // Popular NeoPixel ring size
// When setting up the NeoPixel library, we tell it how many pixels,
// and which pin to use to send signals. Note that for older NeoPixel
// strips you might need to change the third parameter -- see the
// strandtest example for more information on possible values.
Adafruit_NeoPixel Neo_pixels_warm1(Neo_NUMPIXELS_warm1, Neo_PIN_1, NEO_GRBW + NEO_KHZ800); // for the led strip to the right
Adafruit_NeoPixel Neo_pixels_warm2(Neo_NUMPIXELS_warm2, Neo_PIN_1 , NEO_GRBW + NEO_KHZ800);// for the led strip to the left
float deg_0 = 0;
float last_Angle_X = 0;
float Angle_X = 0;
float Vel_X = 0;
////-------------------------------------------------------------------------------------

// Enter a MAC address and IP address for your controller below.
// The IP address will be dependent on your local network:
byte mac[] = {
  0x00, 0xAA, 0xBB, 0xCC, 0xDE, 0x02
  //0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED
};
IPAddress ip(192, 168, 1, 65);   // gives an IP to your Arduino 
unsigned int localPort = 8888;      // local port to listen on
// buffers for receiving and sending data
char packetBuffer[UDP_TX_PACKET_MAX_SIZE];  //buffer to hold incoming packet,
char  ReplyBuffer[] = "acknowledged";       // a string to send back

// An EthernetUDP instance to let us send and receive packets over UDP
EthernetUDP Udp;
//

/////////////
#include "LEDControl.h"
#include "IMU.h"
#include "UDPParsing.h"
void setup() {
  Serial.begin(115200);
  // Setup the pins as Outputs
  Neo_pixels_warm1.clear(); // Set all pixel colors to 'off'
  Neo_pixels_warm1.begin(); // INITIALIZE NeoPixel strip object (REQUIRED)
  Neo_pixels_warm2.clear(); // Set all pixel colors to 'off'
  Neo_pixels_warm2.begin(); // INITIALIZE NeoPixel strip object (REQUIRED)
//----------------
  Wire.begin();
  mpu6050.begin();
  mpu6050.calcGyroOffsets(true);
    Ethernet.begin(mac,ip);
  Udp.begin(localPort);
  Serial.print("Chat server address:");
  Serial.println(Ethernet.localIP());
    // start the Ethernet and UDP
}

void loop() {
  // if there's data available, read a packet
  int packetSize = Udp.parsePacket();
  if (packetSize) {
    Udp.read(packetBuffer, UDP_TX_PACKET_MAX_SIZE);
    String asString(packetBuffer);
    udpParse(asString);
    memset(packetBuffer,0,UDP_TX_PACKET_MAX_SIZE); // clear buffer 
  }
}
