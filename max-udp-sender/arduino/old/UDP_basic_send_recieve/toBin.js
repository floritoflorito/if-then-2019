function msg_int(v)
{
	var out = v.toString(2);
	while (out.length<8) out="0"+out;
	outlet(0, out);
}