
var separator = " ";
if (jsarguments.length>1) {
	separator = jsarguments[1];
}
// post("hoi, separator = \""+separator+"\""+"\n");

function makestring() {
	// post("makestring\n");
	var a = arrayfromargs(arguments);
	// post("received list length "+a.length+": " + a + "\n");
	var out = "";
	for (var i=0;i<a.length;i++) {
		out += a[i];
		if (i<a.length-1) out += separator;
	}
	// post("out: "+out);
	// out += "test";
	outlet(0, out);
}