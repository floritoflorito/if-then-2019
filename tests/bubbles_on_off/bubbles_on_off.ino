// '1' on schematic drawing:
#define BUBBLE_PWM_PIN 6
// '2' on schematic drawing
#define BUBBLE_DIRECTION_PIN 7

#define DELAY 15000

void setup() {
  // put your setup code here, to run once:
  pinMode(BUBBLE_PWM_PIN, OUTPUT);
  pinMode(BUBBLE_DIRECTION_PIN, OUTPUT);
  Serial.begin(115200);
  Serial.println("Bubble demo");
}

void loop() {
  setBridge(LOW, HIGH, DELAY);
  setBridge(HIGH, HIGH, DELAY);
  setBridge(LOW, HIGH, DELAY);
  setBridge(HIGH, HIGH, DELAY);
  setBridge(LOW, LOW, 3000);
}

void setBridge(int dir, int ena, int dly) {
  Serial.print(dly);
  Serial.print("ms: ");
  Serial.print("ena=");
  Serial.print(ena);
  Serial.print(", dir=");
  Serial.println(dir);
  digitalWrite(BUBBLE_DIRECTION_PIN, dir);
  digitalWrite(BUBBLE_PWM_PIN, ena);
  delay(dly);
}
  
